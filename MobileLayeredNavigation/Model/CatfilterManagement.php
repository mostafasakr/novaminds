<?php

namespace NovaMinds\MobileLayeredNavigation\Model;

class CatfilterManagement implements \NovaMinds\MobileLayeredNavigation\Api\CatfilterManagementInterface {

    /**
     * Catalog layer
     *
     * @var \Magento\Catalog\Model\Layer
     */
    protected $_catalogLayer;

    /**
     * @var \Magento\Catalog\Model\Layer\FilterList
     */
    protected $filterList;

/**
 * Escaper
 *
 * @var \Magento\Framework\Escaper
 */
protected $_escaper;


    protected $_request;

    /**
     * @param \Magento\Catalog\Model\Layer\Resolver $layerResolver
     * @param \Magento\Catalog\Model\Layer\FilterList $filterList
     * @param \Magento\Framework\Webapi\Rest\Request $request
     * @param \Magento\Framework\Escaper $_escaper
     * @param array $data
     */
    public function __construct(
            \Magento\Catalog\Model\Layer\Resolver $layerResolver,
            \Magento\Catalog\Model\Layer\FilterList $filterList,
            \Magento\Framework\Webapi\Rest\Request $request,
            \Magento\Framework\Escaper $_escaper,
            array $data = []
    ) {
        $this->_catalogLayer = $layerResolver->get();
        $this->filterList = $filterList;
        $this->_request = $request;
        $this->_escaper = $_escaper;
    }

    /**
     * {@inheritdoc}
     */
    public function getCatfilter() {

        $filters = $this->filterList->getFilters($this->_catalogLayer);

        $finalFilters = [];
        $data = array();
        $i = 0;
        $this->getLayer()->setCurrentCategory(119);
        $this->_prepareLayout();
        foreach ($this->getFilters() as $filter) {
            if ($filter->getItemsCount()) {
                $name = $filter->getRequestVar();
                foreach ($filter->getItems() as $item) {
                    if ($item->getFilter()->hasAttributeModel() && $item->getFilter()->getAttributeModel()->getAttributeCode() == 'price') {
$itemData = array(
    'name' => strip_tags($item->getLabel()),
    'value' => $item->getValue(),
    'count' => $item->getCount(),
//    'code' => $item->getFilter()->getAttributeModel()->getAttributeCode(),
    'max' => $this->getLayer()->getProductCollection()->getMaxPrice(),
    'min' => $this->getLayer()->getProductCollection()->getMinPrice(),



);
$finalFilters[$name]['filter']['code']= $item->getFilter()->getAttributeModel()->getAttributeCode();
$finalFilters[$name]['filter']['value'][]= $itemData;
//array_push($finalFilters[$name],$itemData);
//                        $finalFilters[$name][$i]['name'] = strip_tags($item->getLabel());
//                        $finalFilters[$name][$i]['value'] = $item->getValue();
//                        $finalFilters[$name][$i]['count'] = $item->getCount();
//                        $finalFilters[$name][$i]['code'] = $item->getFilter()->getAttributeModel()->getAttributeCode();
                    } else {
$itemData = array(
    'name' => strip_tags($item->getLabel()),
    'value' => $item->getValue(),
    'count' => $item->getCount(),
//    'code' => $item->getFilter()->getRequestVar()
);
$finalFilters[$name]['filter']['code']= $item->getFilter()->getRequestVar();
$finalFilters[$name]['filter']['value'][]= $itemData;
//                        $finalFilters[$name][$i]['name'] = $item->getLabel();
//                        $finalFilters[$name][$i]['value'] = $item->getValue();
//                        $finalFilters[$name][$i]['code'] = $item->getFilter()->getRequestVar();
//                        $finalFilters[$name][$i]['count'] = $item->getCount();
                    }
                    $i++;
                }
                
         
            }
        }
        

        return $finalFilters;
    }

    /**
     * Apply layer
     *
     * @return $this
     */
    protected function _prepareLayout() {
        foreach ($this->filterList->getFilters($this->_catalogLayer) as $filter) {
            $filter->apply($this->getRequest());
        }
        $this->getLayer()->apply();
        return $this;
    }

    /**
     * Get layer object
     *
     * @return \Magento\Catalog\Model\Layer
     */
    public function getLayer() {
        return $this->_catalogLayer;
    }

    /**
     * Get all layer filters
     *
     * @return array
     */
    public function getFilters() {
        return $this->filterList->getFilters($this->_catalogLayer);
    }

    public function getRequest() {
        return $this->_request;
    }

}
