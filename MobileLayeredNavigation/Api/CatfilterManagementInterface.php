<?php


namespace NovaMinds\MobileLayeredNavigation\Api;

interface CatfilterManagementInterface
{

    /**
     * GET for catfilter api
     * @return string
     */
    public function getCatfilter();
}
