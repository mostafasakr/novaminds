<?php
namespace NovaMinds\CustomsInvoice\Api;
 
interface CustomsInvoiceRepositoryInterface {
    /**
     * Get Customs Invoice by order id
     *
     * @param $orderId
     * @return \NovaMinds\CustomsInvoice\Api\Data\CustomsInvoiceInterface
     */
    public function get($orderId);

    /**
     * Delete  Customs Invoice by order id
     *
     * @param  $orderId
     * @return bool
     */
    public function delete($orderId);

    /**
     * Save   Customs Invoice  for order id
     *
     * @param \Magento\Catalog\Api\Data\ProductCustomOptionInterface $option
     * @return \Magento\Catalog\Api\Data\ProductCustomOptionInterface
     */
    public function save(\NovaMinds\CustomsInvoice\Api\Data\CustomsInvoiceInterface $customsInvoice );
    
}
