<?php
namespace NovaMinds\CustomsInvoice\Api\Data;
 // NovaMinds\CustomsInvoice\Api\Data\CustomsInvoiceInterface
interface CustomsInvoiceInterface {
    
    const ORDER_ID = 'order_id';
    const GRAND_TOTAL='grand_total';
    /**
     * Get orderId
     * @return string|null
     */
    public function getOrderId();
    
    /**
     * Set OrderId
     * @param string $orderId
     * @return  NovaMinds\CustomsInvoice\Api\Data\CustomsInvoiceInterface
     */
    public function setOrderId($orderId);
    

    /**
     * Get Customs price
     * @return float|null
     */
    public function getCustomsPrice();
    
    /**
     * Get Customs currency symbol
     * @return string|null
     */
    public function getCustomsCurrencysymbol();
    
  

    
}
