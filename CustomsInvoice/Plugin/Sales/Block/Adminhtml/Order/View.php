<?php
namespace NovaMinds\CustomsInvoice\Plugin\Sales\Block\Adminhtml\Order;
use Magento\Sales\Block\Adminhtml\Order\View as OrderView;
class View
{
    public function beforeSetLayout(OrderView $subject)
    {
        $subject->addButton(
            'order_custom_button',
            [
                'label' => __('Customs Invoice'),
                'class' => __('customs-invoice'),
                'id' => 'customs-invoice-button',
                'onclick' => 'setLocation(\'' . $subject->getUrl('customsinvoice/customs/index', ['id' => $subject->getOrderId()]) . '\')'
            ]
        );
    }
}
