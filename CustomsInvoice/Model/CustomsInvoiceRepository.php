<?php
namespace NovaMinds\CustomsInvoice\Model;

class CustomsInvoiceRepository implements \NovaMinds\CustomsInvoice\Api\CustomsInvoiceRepositoryInterface
{
 
    protected $orderRepo;
    protected $customsInvoice;
// https://devdocs.magento.com/guides/v2.2/extension-dev-guide/framework/serializer.html 
protected $serializer;
    /**
     * Constructor
     *
     * @param \Magento\Backend\App\Action\Context  $context
     * 
     */
    public function __construct(
       \Magento\Sales\Api\OrderRepositoryInterface $orderRepo,
       \NovaMinds\CustomsInvoice\Api\Data\CustomsInvoiceInterface $customsInvoice,
        \Magento\Framework\Serialize\SerializerInterface  $serializer
    ) {
        $this->orderRepo = $orderRepo;
        $this->customsInvoice = $customsInvoice;
        $this->serializer= $serializer;
    }
    
    
    /**
     * {@inheritdoc}
     */
    public function delete($orderId){
        $order = $this->orderRepo->get($orderId);
        if( $order->getId()){
            $order->setCustomsInvoice ("");
            // var_dump( $order->save()->getCustomsInvoice()."sss" );die;
            $order->save();
            return True;
        }
        return FALSE;
    }

    /**
     * {@inheritdoc}
     */
    public function get($orderId){
        $order = $this->orderRepo->get($orderId);
        $this->customsInvoice->setOrderId($orderId);
        if( $order->getId()){
            $data = $order->getCustomsInvoice ( );
            if(!empty($data)){
                // $this->customsInvoice->
              $unSerialData = $this->serializer->unserialize($data);  
              $this->customsInvoice->setData($unSerialData);
            }
            return $this->customsInvoice;
        }
    }

    /**
     * {@inheritdoc}
     */
    
    public function save(\NovaMinds\CustomsInvoice\Api\Data\CustomsInvoiceInterface $customsInvoice) {
        $orderId = $customsInvoice->getOrderId();
        // var_dump($orderId);die;
        if( $orderId ){
            $order = $this->orderRepo->get($orderId);
            $data = $customsInvoice->getData();
            // var_dump($data);
            $serialData = $this->serializer->serialize($data);
            // var_dump($serialData);
            $order->setCustomsInvoice ( $serialData );
            return $order->save();
        }
        return FALSE;
    }

}
