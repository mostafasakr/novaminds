<?php
namespace NovaMinds\CustomsInvoice\Model\Data;

class CustomsInvoice extends \Magento\Framework\DataObject
                     implements \NovaMinds\CustomsInvoice\Api\Data\CustomsInvoiceInterface
{
      protected $orderRepo;
      
      public function __construct(
       \Magento\Sales\Api\OrderRepositoryInterface $orderRepo
     ) {
        $this->orderRepo = $orderRepo;
    }
    
    
    public function getOrderId() {
        return $this->getData(self::ORDER_ID);
    }

    public function setOrderId($orderId) {
        return $this->setData(self::ORDER_ID,$orderId);
    }

    public function getCustomsPrice() {
        $val = $this->getData(self::GRAND_TOTAL);
        $order = $this->orderRepo->get( $this->getOrderId() );
        if(!isset($val)){
            if($order->getId()){ return $order->getGrandTotal(); }
            return 0;
        }
        if( empty($val)){
            if($order->getId()){ return $order->getGrandTotal(); }
            return 0 ;
        }
       if( $val < 1){
            if($order->getId()){ return $order->getGrandTotal(); }
            return 0 ;
        }
        return $val;
    }
    
    public function isEmpty() {
        $data = $this->getData();
        unset($data[self::ORDER_ID]);
        return !boolval( sizeof($data ) );
    }

    public function getCustomsCurrencysymbol() {
       $order = $this->orderRepo->get( $this->getOrderId() );
       $currencysymbol =  $this->getData( 'currencysymbol' );
       if(empty( $currencysymbol )) {
           if($order->getId()){ return $order->getOrderCurrencyCode(); }
           return "";
       }
       return str_replace("#","", $currencysymbol ); 
    }

}

