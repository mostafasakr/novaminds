<?php
// composer require spiritix/html-to-pdf
// composer require tecnickcom/tcpdf  <<<<
namespace NovaMinds\CustomsInvoice\Controller\Adminhtml\Printer;

class Index extends \Magento\Backend\App\Action {

    protected $resultPageFactory;
    /**
     * @var \Magento\Framework\App\Response\Http\FileFactory
     */
    protected $fileFactory;

    protected $directoryList;
    protected $logo;
    protected $scopeConfig;
    protected $customsInvoiceRepo;

    /**
     * Constructor
     *
     * @param \Magento\Backend\App\Action\Context  $context
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Framework\App\Response\Http\FileFactory $fileFactory,
        \Magento\Framework\App\Filesystem\DirectoryList  $directoryList,
        \Magento\Theme\Block\Html\Header\Logo $logo,
        \NovaMinds\CustomsInvoice\Api\CustomsInvoiceRepositoryInterface $customsInvoiceRepo ,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig 
    ) {
        $this->fileFactory = $fileFactory;
        $this->resultPageFactory = $resultPageFactory;
        $this->directoryList = $directoryList;
        $this->logo = $logo;
        $this->scopeConfig =$scopeConfig;
        $this->customsInvoiceRepo = $customsInvoiceRepo;
         // \Magento\Framework\App\Filesystem\DirectoryList directoryList()
        parent::__construct($context);
    }

    private function createPDF(  $passedHTML = "", $invoice = "Invoice #xxxx", $invoiceText = "" , $footerNotice = ""){
      // create new PDF document
      // $pdf = new \TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
      $pdf = new NmcitPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

      // set document information
      $pdf->SetCreator(PDF_CREATOR);
      $pdf->SetAuthor('Herbsgate.com');
      $pdf->SetTitle('Invoice');
      $pdf->SetSubject('Invoice sheet');
      $pdf->SetKeywords('Invoice, Invoice sheet');

//      $customLogo = $this->config->getCurrentScope()->getValue('sales/customsinvoice/customsinvoicelogogroup/logo');
//       
//      die( $customLogo );
      
      $pubMediaDir = $this->directoryList->getPath(\Magento\Framework\App\Filesystem\DirectoryList::MEDIA);
       // $logoSrc = $pubMediaDir."/logo/default/".basename ( $this->logo->getLogoSrc() );
      $logoSrc = "../../../../../pub/media/logo/default/".basename ( $this->logo->getLogoSrc() );
      // $logoSrc = $this->logo->getLogoSrc();

       // var_dump( $this->logo->getLogoSrc() , basename ( $this->logo->getLogoSrc() ) );die;
       // var_dump($logoSrc);die;
      // set default header data
      $pdf->SetHeaderData( $logoSrc ,
                           PDF_HEADER_LOGO_WIDTH,
                           $invoice ,
                           $invoiceText,
                           array(0,64,255),
                           array(0,64,128));

      // remove default header/footer
       $pdf->setPrintHeader(false);
       // $pdf->setPrintFooter(false);
       $pdf->setFooterText($footerNotice);
      // $pdf->setFooterData(array(0,64,0), array(0,64,128));

      // $footerNotice

      // set header and footer fonts
      $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
      $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
      // set default monospaced font
      $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

      /// set margins
      $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
      $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
      $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

      // $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
      $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
      // set auto page breaks
      $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
      // set image scale factor
      $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
      // set some language-dependent strings (optional)
      if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
        require_once(dirname(__FILE__).'/lang/eng.php');
        $pdf->setLanguageArray($l);
      }
      // ---------------------------------------------------------

      // set default font subsetting mode
      $pdf->setFontSubsetting(true);
      // Set font
      // dejavusans is a UTF-8 Unicode font, if you only need to
      // print standard ASCII chars, you can use core fonts like
      // helvetica or times to reduce file size.
      $pdf->SetFont('dejavusans', '', 12, '', true);
      // Add a page
      // This method has several options, check the source code documentation for more information.
      $pdf->AddPage();
      // set text shadow effect
      $pdf->setTextShadow(array('enabled'=>true, 'depth_w'=>0.2, 'depth_h'=>0.2, 'color'=>array(196,196,196), 'opacity'=>1, 'blend_mode'=>'Normal'));
      // Set some content to print
      $html =$passedHTML ;
      // Print text using writeHTMLCell()
      // $pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);
      $pdf->writeHTML($html);
      // ---------------------------------------------------------
      // Close and output PDF document
      // This method has several options, check the source code documentation for more information.
      // echo $pdf->Output('example_001.pdf', 'I');

      //============================================================+
      // END OF FILE
      //============================================================+
      return $pdf->Output('example_001.pdf', 'S') ;
    }

    /**
     * Execute view action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute() {
        // $paramsArr = $this->getRequest()->getParams();
        // print_r($paramsArr["Invoice"]);
//        $orderId = $paramsArr["Invoice"]["orderid"];
//        $customInvoice = $this->customsInvoiceRepo->get($orderId);
//        $customInvoice->setData("ID","11254545");
//        $customInvoice->setData("Helal","Ahmed Helal");
        // $customInvoice->add("dsdsd");
        // $customInvoice->setHelal("helal");
//        $this->customsInvoiceRepo->save($customInvoice);
        // var_dump(  $customInvoice->getData()  );
        // die("eeeee  ".$orderId);
       $customLogoDir = '/custominvoice/logo';
       $customLogo = $this->scopeConfig->getValue('customsinvoice/customsinvoicelogogroup/customsinvoicelogo_logo', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
       $customLogoPath = $customLogoDir.'/'.$customLogo;       
      
      // $this->getRequest()->getParam('id');
      $paramsArr = $this->getRequest()->getParams();
      
      $orderId = $paramsArr["Invoice"]["order_id"];
      $customInvoice = $this->customsInvoiceRepo->get($orderId);
      // print_r($customInvoice->getCustomsPrice());die; 
      
      
       //print_r( $paramsArr );die;
      // var_dump( $paramsArr['Invoice']['increment_id'] );die;
      $customInvoice->setData( $paramsArr['Invoice'] );

      $flage = $this->customsInvoiceRepo->save( $customInvoice );
      // print_r($customInvoice->getCustomsPrice());die;
      // var_dump($customInvoice->isEmpty());die; 
      
      $pubMediaDir = $this->directoryList->getPath(\Magento\Framework\App\Filesystem\DirectoryList::MEDIA);
      $image = $pubMediaDir."/wysiwyg/nmcit/logo.png";
      
      if(!empty($customLogo)){
        $logoSrc = $pubMediaDir.$customLogoPath;
      }else{
        $logoSrc = $this->logo->getLogoSrc();
      }

      $discount = "";
      if(!empty(trim($paramsArr['Invoice']['discountamount']))){
        $discount = '<span> <strong>'.$paramsArr['Invoice']['discountlabel'].':</strong> '.
                str_replace('#' , number_format((float)$paramsArr['Invoice']['discountamount'], 2, '.', '') , $paramsArr['Invoice']['currencysymbol'])
                    .'</span><br>';
      }
      $productHtml='';
      $grandtotal = 0;
      foreach ( $paramsArr['Invoice']['products'] as $product) {
        $productHtml.= '<tr>
                          <td style="width: 300px">'.$product['name'].'</td>
                          <td style="width: 100px">'.$product['sku'].'</td>
                          <td style="width: 100px">'.$product['qty'].'</td>
                          <td style="width: 100px">'.str_replace('#' , number_format((float)$product['price'], 2, '.', '') , $paramsArr['Invoice']['currencysymbol']).'</td>
                        </tr>';
      }
      if(empty($paramsArr['Invoice']['discountamount'])){
          $grandtotal = $paramsArr['Invoice']['grand_total'];
      }else{
          // $grandtotal = $paramsArr['Invoice']['grand_total'] - $paramsArr['Invoice']['discountamount'];
          $grandtotal = $paramsArr['Invoice']['grand_total'] ;
      }
      $date = new \DateTime($paramsArr['Invoice']['created_at']);
      
      // $invoice = "Invoice #".$paramsArr['Invoice']['increment_id'] , "Created at: ".$paramsArr['Invoice']['created_at']
      $html = '<style>
                  .invoice-data{display: inline-block;text-align: right;}
                  .invoice-data p { margin-bottom:0;margin-top:0}
                  .invoice-address{padding:0}
                  .invoice-table{border:1px solid #A3A3A3}
                  .invoice-table td{padding:5px;}
                  .td-head{background-color:#A3A3A3;color:#000;}
                  .invoice-totals-content{text-align:right}
                  .text-right{text-align:right}
                  .text-center{text-align:center}
                  .invoice-title{font-size: 65px;line-height: 1;}
                  .idp{padding:5px; background-color:#A3A3A3; min-hieght:60px}
                  .logo-img{height:60px;}
              </style>
      <div class="container">
                  <div class="invoice-header">
                      <table>
                          <tr>
                              <td>
                                <img class="logo-img" src="'.$logoSrc.'" />
                              </td>
                              <td>
                                <table>
                                    <tr>
                                        <td>
                                            <span class="invoice-title"> Invoice </span><br>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="padding-left: 100px">
                                          <span class="invoice-header-date"><strong>Date: </strong> '.$date->format('Y-m-d').' </span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="padding-left: 100px">
                                          <span class="invoice-header-num"><strong>Invoice #: </strong> '.$paramsArr['Invoice']['increment_id'].' </span>
                                        </td>
                                    </tr>
                                </table>

                              </td>
                          </tr>
                      </table>
                  </div>
                  <div class="invoice-address"  >
                    <table>
                      <tr>
                          <td width="50"> To: </td>
                          <td>'. str_replace('T: ' , '' , preg_replace('/(?:(?:\r\n|\r|\n)\s*){2}/s', "<br>", $paramsArr['Invoice']['shippingaddress'] )) .' </td>
                      </tr>
                    </table>
                  </div>

                  <div class="invoice-products">
                      <table class="invoice-table" cellpadding="7" border="1">
                          <tr>
                              <td class="td-head" style="width: 300px">Product</td>
                              <td class="td-head" style="width: 100px">SKU</td>
                              <td class="td-head" style="width: 100px">Qty</td>
                              <td class="td-head" style="width: 100px">Price</td>
                          </tr>'.$productHtml.'
                          <tr>
                            <td  colspan="4" align="right">
                                <span class="table-totals" style="text-algin:left">
                                    <span> <strong>Subtotal: </strong>'.
                                        str_replace('#' , number_format((float)$paramsArr['Invoice']['subtotal'], 2, '.', '') , $paramsArr['Invoice']['currencysymbol']) 
                                        .'</span><br>
                                    <span> <strong>Shipping&Handling: </strong>'.
                                        str_replace('#' , $paramsArr['Invoice']['base_shipping_amount'] , $paramsArr['Invoice']['currencysymbol'])
                                    .'</span><br>
                                    '.$discount.'
                                    <span> <strong>Grand Total: '.
                                        str_replace('#' , number_format((float)$grandtotal, 2, '.', '') , $paramsArr['Invoice']['currencysymbol'])
                                    .'</strong></span><br>
                                </span>
                            </td>
                          </tr>
                          </table>
                  </div>
                  <div class="invoice-thanks text-center">
                    <span> '.$paramsArr['Invoice']['thankyoumessage'].' </span>
                  </div>
               </div>';
      $fileName = 'invoice#'.$paramsArr['Invoice']['increment_id'].'.pdf' ;
      $this->fileFactory->create(
         $fileName,
         $this->createPDF(  $html , $invoice = "Invoice #".$paramsArr['Invoice']['increment_id'] , "Created at: ".$paramsArr['Invoice']['created_at'],$paramsArr['Invoice']['invoicefooternotice']) ,
         \Magento\Framework\App\Filesystem\DirectoryList::VAR_DIR, // this pdf will be saved in var directory with the name example.pdf
         'application/pdf'
      );


    }
}


// Extend the TCPDF class to create custom Header and Footer
class NmcitPDF extends \TCPDF {

  private $footerText="";
  public function getFooterText(){
    return $this->footerText;
  }
  public function setFooterText( $value ){
      $this->footerText = $value;
  }
	//Page header
	public function Header() {
		// Logo
		$image_file = K_PATH_IMAGES.'logo_example.jpg';
		$this->Image($image_file, 10, 10, 15, '', 'JPG', '', 'T', false, 300, '', false, false, 0, false, false, false);
		// Set font
		$this->SetFont('helvetica', 'B', 20);
		// Title
		$this->Cell(0, 15, '<< TCPDF Example 003 >>', 0, false, 'C', 0, '', 0, false, 'M', 'M');
	}

	// Page footer
	public function Footer() {
		// Position at 15 mm from bottom
		$this->SetY(-15);
		// Set font
		$this->SetFont('helvetica', 'I', 8);


    // Page Footer text
    $footerText= $this-> getFooterText();
    if(!empty($footerText)){
      $this->Cell(0, 10, $footerText , 0, false, 'C', 0, '', 0, false, 'T', 'M');
    }
		// Page number
		$this->Cell(0, 10, 'Page '.$this->getAliasNumPage().'/'.$this->getAliasNbPages(), 0, false, 'C', 0, '', 0, false, 'T', 'M');
	}
}
