<?php

namespace NovaMinds\CustomsInvoice\Controller\Adminhtml\Customs;

class Index extends \Magento\Backend\App\Action {

    protected $resultPageFactory;
    protected $orderRepository;
    protected $_coreRegistry;

    /**
     * Constructor
     *
     * @param \Magento\Backend\App\Action\Context  $context
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     */
    public function __construct(
    \Magento\Backend\App\Action\Context $context, \Magento\Sales\Api\OrderRepositoryInterface $orderRepository, \Magento\Framework\Registry $registry, \Magento\Framework\View\Result\PageFactory $resultPageFactory
    ) {
        $this->resultPageFactory = $resultPageFactory;
        $this->orderRepository = $orderRepository;
        $this->_coreRegistry = $registry;
        parent::__construct($context);
    }

    /**
     * Execute view action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute() {

        $Id = $this->getRequest()->getParam('id');
        if ($Id) {
            $model = $this->orderRepository->get($Id);
            $this->_coreRegistry->register('customs_invoice', $model);
            if (!$model->getId()) {
                $this->messageManager->addError(__('This Invoice no longer exists.'));
                $this->_redirect('*/*/');
                return;
            }
        }


        // $this->_coreRegistry->register('customs_invoice', $model);

        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->resultPageFactory->create();
        $resultPage->getConfig()->getTitle()->prepend(__('Simple Invoice'));

        return $resultPage;


//        return $this->resultPageFactory->create();
    }

    protected function _isAllowed() {
//            return true;
        return $this->_authorization->isAllowed('NovaMinds_CustomsInvoice::Customs');
    }

}
