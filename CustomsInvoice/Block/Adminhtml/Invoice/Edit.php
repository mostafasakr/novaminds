<?php

namespace NovaMinds\CustomsInvoice\Block\Adminhtml\Invoice;

use Magento\Backend\Block\Widget\Form\Container;
use Magento\Backend\Block\Widget\Context;
use Magento\Framework\Registry;

class Edit extends Container
{
   /**
     * Core registry
     *
     * @var \Magento\Framework\Registry
     */
    protected $_coreRegistry = null;

    /**
     * @param Context $context
     * @param Registry $registry
     * @param array $data
     */
    public function __construct(
        Context $context,
        Registry $registry,
        array $data = []
    ) {
        $this->_coreRegistry = $registry;
        parent::__construct($context, $data);
    }

    /**
     * Class constructor
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_objectId = 'id';
        $this->_controller = 'adminhtml_Invoice';
        $this->_blockGroup = 'NovaMinds_CustomsInvoice';

        parent::_construct();

        // $this->buttonList->update('save', 'label', __('Save'));
        // $this->buttonList->add(
        //     'nmcitprintinvoice',
        //     [
        //         'label' => __('Print'),
        //         'class' => 'save',
        //         'data_attribute' => [
        //             'mage-init' => [
        //                 'button' => [
        //                     'event' => 'nmcitprintinvoice',
        //                     'target' => '#edit_form'
        //                 ]
        //             ]
        //         ]
        //     ],
        //     -100
        // );
        // $this->buttonList->update('delete', 'label', __('Delete'));
        $this->buttonList->remove('delete');
        $this->buttonList->remove('reset');
        $this->buttonList->remove('save');
        $this->buttonList->update('back', 'onclick', 'setLocation(\'' . $this->getUrl('sales/order/view', ['order_id' => $this->getRequest()->getParam('id') ]) . '\')' );

        $this->buttonList->add(
            'nmcitprintinvoice',
            [
                'label' => __('Print'),
                'onclick' => 'setLocation(\'' . $this->getUrl('customsinvoice/printer/index') . '\')',
                'class' => 'action action-print',
                'style' => 'display:none;'
            ],
            -100
        );

        $this->buttonList->add(
            'nmcitprintinvoicesubmit',
            [
                'label' => __('Print'),
                'onclick' => "jQuery('#edit_form').submit();",
                'class' => 'action action-print'
            ],
            -100
        );


    }

    /**
     * Retrieve text for header element depending on loaded Invoice
     *
     * @return string
     */
    public function getHeaderText()
    {
        $InvoiceRegistry = $this->_coreRegistry->registry('customs_invoice');
        if ($InvoiceRegistry->getId()) {
            $InvoiceTitle = $this->escapeHtml($InvoiceRegistry->getTitle());
            return __("Edit Invoice '%1'", $InvoiceTitle);
        } else {
            return __('Add Invoice');
        }
    }

    /**
     * Prepare layout
     *
     * @return \Magento\Framework\View\Element\AbstractBlock
     */
    protected function _prepareLayout()
    {
        $this->_formScripts[] = "
            // nmcit
            function toggleEditor() {
                if (tinyMCE.getInstanceById('post_content') == null) {
                    tinyMCE.execCommand('mceAddControl', false, 'post_content');
                } else {
                    tinyMCE.execCommand('mceRemoveControl', false, 'post_content');
                }
            };
        ";

        return parent::_prepareLayout();
    }
}
