<?php

namespace NovaMinds\CustomsInvoice\Block\Adminhtml\Invoice\Edit;

use Magento\Backend\Block\Widget\Tabs as WidgetTabs;

class Tabs extends WidgetTabs
{
    /**
     * Class constructor
     *
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setId('Invoice_edit_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(__('Invoice Information'));
    }

    /**
     * @return $this
     */
    protected function _beforeToHtml()
    {
        $this->addTab(
            'Invoice_info',
            [
                'label' => __('General'),
                'title' => __('General'),
                'content' => $this->getLayout()->createBlock(
                    'NovaMinds\CustomsInvoice\Block\Adminhtml\Invoice\Edit\Tab\Info'
                )->toHtml(),
                'active' => true
            ]
        );

        $this->addTab(
            'Invoice_items',
            [
                'label' => __('Items'),
                'title' => __('Items'),
                'content' => $this->getLayout()->createBlock(
                    'NovaMinds\CustomsInvoice\Block\Adminhtml\Invoice\Edit\Tab\Items'
                )->toHtml(),
                'active' => true
            ]
        );


        return parent::_beforeToHtml();
    }
}
