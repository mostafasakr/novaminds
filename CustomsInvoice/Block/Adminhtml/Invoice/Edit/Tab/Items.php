<?php

namespace NovaMinds\CustomsInvoice\Block\Adminhtml\Invoice\Edit\Tab;

use Magento\Backend\Block\Widget\Form\Generic;
use Magento\Backend\Block\Widget\Tab\TabInterface;
use Magento\Backend\Block\Template\Context;
use Magento\Framework\Registry;
use Magento\Framework\Data\FormFactory;
use Magento\Cms\Model\Wysiwyg\Config;

class Items extends Generic implements TabInterface
{
    /**
     * @var \Magento\Cms\Model\Wysiwyg\Config
     */
    protected $_wysiwygConfig;
    protected $_productRepository;
     protected $scopeConfig;
     protected $customsInvoiceRepo;

    /**
     * @var \NovaMinds\CustomsInvoice\Model\Config\Status
     */

   /**
     * @param Context $context
     * @param Registry $registry
     * @param FormFactory $formFactory
     * @param Config $wysiwygConfig
     * @param Status $InvoiceStatus
     * @param array $data
     */
    public function __construct(
        \Magento\Catalog\Model\ProductRepository $productRepository,
        Context $context,
        Registry $registry,
        FormFactory $formFactory,
        Config $wysiwygConfig,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig ,
        \NovaMinds\CustomsInvoice\Api\CustomsInvoiceRepositoryInterface $customsInvoiceRepo ,
        array $data = []
    ) {
        $this->_wysiwygConfig = $wysiwygConfig;
        $this->_productRepository = $productRepository;
        $this->scopeConfig =$scopeConfig;
        $this->customsInvoiceRepo = $customsInvoiceRepo;
        parent::__construct($context, $registry, $formFactory, $data);
    }

    /**
     * Prepare form fields
     *
     * @return \Magento\Backend\Block\Widget\Form
     */
    protected function _prepareForm()
    {
       /** @var $model \NovaMinds\CustomsInvoice\Model\Invoice */
        $model = $this->_coreRegistry->registry('customs_invoice');
        $tmp = $model->getData();
        
        
        
        $priceReductionvalue = $this->scopeConfig->getValue(
                                    'customsinvoice/customsinvoicedata/precent',
                                    \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        $priceReductionPrecent = 1;
        if( !empty( $priceReductionvalue ) ){
            $tmpval = intval($priceReductionvalue);
            if( $tmpval > 0 ){
                $priceReductionPrecent = $tmpval/100;
            }
        }
        

        /** @var \Magento\Framework\Data\Form $form */
        $form = $this->_formFactory->create();
        $form->setHtmlIdPrefix('Invoice_');
        $form->setFieldNameSuffix('Invoice');

        $fieldset = $form->addFieldset(
            'base_fieldset',
            ['legend' => __('Items')]
        );

        if ($model->getId()) {
            $fieldset->addField(
                'id',
                'hidden',
                ['name' => 'id']
            );
        }

//--------
        $fieldset->addField(
          "header_name" ,
          'text',
          [
              'name'     => 'header_name',
              'value'    => __( 'Name' ) ,
              'class'    => 'invoice-header-row',
              'disabled' => 'disabled',
              'id'       => 'nameColumn'

          ]
        );

        $fieldset->addField(
          "header_sku" ,
          'text',
          [
              'name'     => 'header_sku',
              'value'    =>  __( 'SKU' ),
              'class'    => 'invoice-header-row',
              'disabled'   =>'disabled'

          ]
        );

        $fieldset->addField(
          "header_quantity" ,
          'text',
          [
              'name'     => 'header_quantity',
              'value'    => __( 'Quantity' ),
              'class'    => 'invoice-header-row',
              'disabled'   =>'disabled'

          ]
        );
        $fieldset->addField(
          "header_price" ,
          'text',
          [
              'name'     => 'header_price',
              'value'    => __( 'Price' ),
              'class'    => 'invoice-header-row',
              'disabled'   =>'disabled'
          ]
        );
        
        $fieldset->addField(
          "price_type" ,
          'text',
          [
              'name'     => 'price_type',
              'value'    => __( 'Price Type' ),
              'class'    => 'invoice-header-row last',
              'disabled'   =>'disabled'
          ]
        );
//---------------
        $orderCustomsInvoice = $this->customsInvoiceRepo->get( $model->getId() );
        
        if( $orderCustomsInvoice->isEmpty()){
            $this->prepareNewFormFields( $fieldset , $tmp ,$priceReductionPrecent );
        }else{
           $this->prepareLoadedFormFields( $fieldset , $orderCustomsInvoice ,$tmp , $priceReductionPrecent ); 
        }
        
        
        // $data = $model->getData();
        // $form->setValues($data);
        $this->setForm($form);

        return parent::_prepareForm();
    }
    
    
    
    private function getOrderCIProduct( $orderCustomsInvoice , $nmcitID ){
        $orderCustomsInvoiceData = $orderCustomsInvoice->getData();
        $orderCustomsInvoiceProducts = $orderCustomsInvoiceData ['products'];  
        
        foreach ($orderCustomsInvoiceProducts as $product ) {
            if( $product['nmcitid'] == $nmcitID ){
                return $product;
            }
        }
        return FALSE;
    }
     private function  prepareLoadedFormFields( $fieldset , $orderCustomsInvoice ,$tmp , $priceReductionPrecent ){

        $list = 1 ;
        foreach ( $tmp[ 'items' ] as $item ) {
          $zzz = $item ->getData();
          // var_dump( array_keys( $zzz ) );die;

          $product = $item->getProduct();
          $tmpID = 'product_'.$product->getId().'_'.$item->getId();
          $prod = $this->_productRepository->getById($product->getId(), false, 0);
          $fieldset->addField(
              $tmpID.'_nmcitid',
              'hidden',
              ['name' => 'products['.$list.'][nmcitid]','value'=>$tmpID]
          );

          
          $orderCustomsInvoiceProduct = $this->getOrderCIProduct( $orderCustomsInvoice , $tmpID );
          
          if( $orderCustomsInvoiceProduct != false ){
              
            $fieldset->addField(
              $tmpID."_name" ,
              'text',
              [
                  'name'     => "products[".$list."][name]" ,
                  'value'    => $orderCustomsInvoiceProduct['name'] ,
                //   'note'     => 'Product Name',
                  'class'    => 'invoice-table-row'
              ]
            );

            $fieldset->addField(
              $tmpID."_sku" ,
              'text',
              [
                  'name'     => "products[".$list."][sku]" ,
                  'value'    => $orderCustomsInvoiceProduct['sku'],
                //   'note'     => 'Product SKU',
                  'class'    => 'invoice-table-row'

              ]
            );
           $fieldset->addField(
              $tmpID."_id" ,
              'hidden',
              [
                  'name'     => "products[".$list."][id]" ,
                  'value'    => $product->getId(),

              ]
            );


            $fieldset->addField(
              $tmpID."_quantity" ,
              'text',
              [
                  'name'     => "products[".$list."][qty]" ,
                  'value'    => $orderCustomsInvoiceProduct['qty'],
                //   'note'     => 'Item quantity',
                  'class'    => 'invoice-table-row invoice-row-action-qty'

              ]
            );



           $fieldset->addField(
              $tmpID."_price" ,
              'text',
              [
                  'name'     => "products[".$list."][price]" ,
                  'value'    => $orderCustomsInvoiceProduct['price'],
                //   'note'     => 'product price',
                  'class'    => 'invoice-table-row invoice-row-action-price'

              ]
            );

            $fieldset->addField(
                  $tmpID."price_type" ,
                  'text',
                  [
                      'name'     => $tmpID.'price_type',
                      'value'    => __( 'Loaded Price' ),
                      'class'    => 'invoice-table-row',
                      'disabled'   =>'disabled'
                  ]
            );
              
          }else{
              // product has no data saved 
              
            $customsName= $product->getData ('customs_name');
            $productName ="";
            if (empty($customsName)) {
              $productName = $product->getName();
            }else{
              $productName = $customsName;
            }

            $fieldset->addField(
              $tmpID."_name" ,
              'text',
              [
                  'name'     => "products[".$list."][name]" ,
                  'value'    => $productName ,
                //   'note'     => 'Product Name',
                  'class'    => 'invoice-table-row'
              ]
            );

            $fieldset->addField(
              $tmpID."_sku" ,
              'text',
              [
                  'name'     => "products[".$list."][sku]" ,
                  'value'    => $product->getSKU(),
                //   'note'     => 'Product SKU',
                  'class'    => 'invoice-table-row'

              ]
            );
           $fieldset->addField(
              $tmpID."_id" ,
              'hidden',
              [
                  'name'     => "products[".$list."][id]" ,
                  'value'    => $product->getId(),

              ]
            );


            $fieldset->addField(
              $tmpID."_quantity" ,
              'text',
              [
                  'name'     => "products[".$list."][qty]" ,
                  'value'    => (int)$item->getQtyOrdered(),
                //   'note'     => 'Item quantity',
                  'class'    => 'invoice-table-row invoice-row-action-qty'

              ]
            );



           $flag = true;
           $customsPrice= $prod->getData ('customs_price');
           $productPrice = 0 ;
           if (empty($customsPrice)) {
             // $productPrice = $prod->getPrice();

              $tmpProductPrice = $prod->getPrice();
              if( $priceReductionPrecent < 1){
                $productPrice =  $tmpProductPrice - ( $tmpProductPrice  * $priceReductionPrecent );
              }else { // no entry or 100% and up 200%
                $productPrice =  $tmpProductPrice;
              }

             $flag = false;
           }else{
             $productPrice = $customsPrice;
           }

            $fieldset->addField(
              $tmpID."_price" ,
              'text',
              [
                  'name'     => "products[".$list."][price]" ,
                  'value'    => number_format((float)$productPrice, 2, '.', ''),
                //   'note'     => 'product price',
                  'class'    => 'invoice-table-row invoice-row-action-price'

              ]
            );

              if($flag){
                  $fieldset->addField(
                      $tmpID."price_type" ,
                      'text',
                      [
                          'name'     => $tmpID.'price_type',
                          'value'    => __( 'Custome Price' ),
                          'class'    => 'invoice-table-row',
                          'disabled'   =>'disabled'
                      ]
                  );
              }else{
                  $fieldset->addField(
                      $tmpID."price_type" ,
                      'text',
                      [
                          'name'     => $tmpID.'price_type',
                          'value'    => __( 'Dollar Price' ),
                          'class'    => 'invoice-table-row',
                          'disabled'   =>'disabled'
                      ]
                  );
              }
            
            }
              

          $list++;

        }
        
    } 
    
    
    private function  prepareNewFormFields( $fieldset , $tmp , $priceReductionPrecent ){
        
        $list = 1 ;
        foreach ( $tmp[ 'items' ] as $item ) {
          $zzz = $item ->getData();
          // var_dump( array_keys( $zzz ) );die;

          $product = $item->getProduct();
          $tmpID = 'product_'.$product->getId().'_'.$item->getId();
          $prod = $this->_productRepository->getById($product->getId(), false, 0);
          $fieldset->addField(
              $tmpID.'_nmcitid',
              'hidden',
              ['name' => 'products['.$list.'][nmcitid]','value'=>$tmpID]
          );


          $customsName= $product->getData ('customs_name');
          $productName ="";
          if (empty($customsName)) {
            $productName = $product->getName();
          }else{
            $productName = $customsName;
          }

          $fieldset->addField(
            $tmpID."_name" ,
            'text',
            [
                'name'     => "products[".$list."][name]" ,
                'value'    => $productName ,
              //   'note'     => 'Product Name',
                'class'    => 'invoice-table-row'
            ]
          );

          $fieldset->addField(
            $tmpID."_sku" ,
            'text',
            [
                'name'     => "products[".$list."][sku]" ,
                'value'    => $product->getSKU(),
              //   'note'     => 'Product SKU',
                'class'    => 'invoice-table-row'

            ]
          );
         $fieldset->addField(
            $tmpID."_id" ,
            'hidden',
            [
                'name'     => "products[".$list."][id]" ,
                'value'    => $product->getId(),

            ]
          );
                    

          $fieldset->addField(
            $tmpID."_quantity" ,
            'text',
            [
                'name'     => "products[".$list."][qty]" ,
                'value'    => (int)$item->getQtyOrdered(),
              //   'note'     => 'Item quantity',
                'class'    => 'invoice-table-row invoice-row-action-qty'

            ]
          );



         $flag = true;
         $customsPrice= $prod->getData ('customs_price');
         $productPrice = 0 ;
         if (empty($customsPrice)) {
           // $productPrice = $prod->getPrice();
             
            $tmpProductPrice = $prod->getPrice();
            if( $priceReductionPrecent < 1){
              $productPrice =  $tmpProductPrice - ( $tmpProductPrice  * $priceReductionPrecent );
            }else { // no entry or 100% and up 200%
              $productPrice =  $tmpProductPrice;
            }

           $flag = false;
         }else{
           $productPrice = $customsPrice;
         }

          $fieldset->addField(
            $tmpID."_price" ,
            'text',
            [
                'name'     => "products[".$list."][price]" ,
                'value'    => number_format((float)$productPrice, 2, '.', ''),
              //   'note'     => 'product price',
                'class'    => 'invoice-table-row invoice-row-action-price'

            ]
          );
          
            if($flag){
                $fieldset->addField(
                    $tmpID."price_type" ,
                    'text',
                    [
                        'name'     => $tmpID.'price_type',
                        'value'    => __( 'Custome Price' ),
                        'class'    => 'invoice-table-row',
                        'disabled'   =>'disabled'
                    ]
                );
            }else{
                $fieldset->addField(
                    $tmpID."price_type" ,
                    'text',
                    [
                        'name'     => $tmpID.'price_type',
                        'value'    => __( 'Dollar Price' ),
                        'class'    => 'invoice-table-row',
                        'disabled'   =>'disabled'
                    ]
                );
            }
              

          $list++;

        }
        
    }

    /**
     * Prepare label for tab
     *
     * @return string
     */
    public function getTabLabel()
    {
        return __('Invoice Items');
    }

    /**
     * Prepare title for tab
     *
     * @return string
     */
    public function getTabTitle()
    {
        return __('Invoice Items');
    }

    /**
     * {@inheritdoc}
     */
    public function canShowTab()
    {
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function isHidden()
    {
        return false;
    }
}
