<?php

namespace NovaMinds\CustomsInvoice\Block\Adminhtml\Invoice\Edit\Tab;

use Magento\Backend\Block\Widget\Form\Generic;
use Magento\Backend\Block\Widget\Tab\TabInterface;
use Magento\Backend\Block\Template\Context;
use Magento\Framework\Registry;
use Magento\Framework\Data\FormFactory;
use Magento\Cms\Model\Wysiwyg\Config;

class Info extends Generic implements TabInterface
{
    /**
     * @var \Magento\Cms\Model\Wysiwyg\Config
     */
    protected $_wysiwygConfig;
    protected $_productRepository;
    protected $scopeConfig;
    protected $customsInvoiceRepo;

    /**
     * @var \NovaMinds\CustomsInvoice\Model\Config\Status
     */

   /**
     * @param Context $context
     * @param Registry $registry
     * @param FormFactory $formFactory
     * @param Config $wysiwygConfig
     * @param Status $InvoiceStatus
     * @param array $data
     */
    public function __construct(
        \Magento\Catalog\Model\ProductRepository $productRepository,
        Context $context,
        Registry $registry,
        FormFactory $formFactory,
        Config $wysiwygConfig,
        \Magento\Sales\Model\Order\Address\Renderer $render ,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig ,
        \NovaMinds\CustomsInvoice\Api\CustomsInvoiceRepositoryInterface $customsInvoiceRepo ,
        array $data = []
    ) {
        $this->_wysiwygConfig = $wysiwygConfig;
        $this->render = $render;
        $this->_productRepository = $productRepository;
        $this->scopeConfig =$scopeConfig;
        $this->customsInvoiceRepo = $customsInvoiceRepo;

        parent::__construct($context, $registry, $formFactory, $data);
    }

    /**
     * Prepare form fields
     *
     * @return \Magento\Backend\Block\Widget\Form
     */
    protected function _prepareForm()
    {
        /** @var $model \NovaMinds\CustomsInvoice\Model\Invoice */
        $model = $this->_coreRegistry->registry('customs_invoice');
        $orderCustomsInvoice = $this->customsInvoiceRepo->get( $model->getId() );
        
        if( $orderCustomsInvoice->isEmpty()){
            $this->prepareNewForm( $model );
        }else{
           $this->prepareLoadedForm( $orderCustomsInvoice , $model); 
        }
      
        
        return parent::_prepareForm();
    }

    private function prepareLoadedForm( $orderCustomsInvoice , $model ){
        /** @var $model \NovaMinds\CustomsInvoice\Model\Invoice */
        // $model = $this->_coreRegistry->registry('customs_invoice');
         $tmp = $model->getData();
         $keys =  array_keys( $tmp );
        /** @var \Magento\Framework\Data\Form $form */
        $subtotal = 0;
        $grandtotal = 0;
        
        $orderCustomsInvoiceData = $orderCustomsInvoice->getData();
                
        //  print_r($orderCustomsInvoiceData ); die;
        
//        $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/customsinvoice.log');
//        $logger = new \Zend\Log\Logger();
//        $logger->addWriter($writer);
//        $logger->info( print_r($orderCustomsInvoiceData , 1) );

        
        $priceReductionvalue = $this->scopeConfig->getValue(
                                    'customsinvoice/customsinvoicedata/precent',
                                    \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        // var_dump($priceReductionvalue);die;
        $priceReductionPrecent = 1;
        if( !empty( $priceReductionvalue ) ){
            $tmpval = intval($priceReductionvalue);
            if( $tmpval > 0 ){
                $priceReductionPrecent = $tmpval/100;
            }
        }
        
        
        // var_dump( $tmp[ 'items' ] );die;
        
        foreach ( $tmp[ 'items' ] as $item ) {
            if( $orderCustomsInvoiceData ){
                $product = $item->getProduct();
                $prod = $this->_productRepository->getById($product->getId(), false, 0);
                $customsPrice= $prod->getData ('customs_price');
                $productPrice = 0 ;
                if (empty($customsPrice)) {
                  $tmpProductPrice = $prod->getPrice();
                  if( $priceReductionPrecent < 1){
                    $productPrice =  $tmpProductPrice - ( $tmpProductPrice  * $priceReductionPrecent );
                  }else { // no entry or 100% and up 200%
                    $productPrice =  $tmpProductPrice;
                  }
                }else{
                  $productPrice = $customsPrice;
                }
                $quantity = $item->getQtyOrdered();
                $subtotal += $productPrice * $quantity;
            }else{
            }
         }
         $subtotal = number_format((float)$subtotal , 2, '.', '');
         $grandtotal = number_format((float)$subtotal + $tmp['base_shipping_amount'], 2, '.', '');
        
        
        $form = $this->_formFactory->create();
        $form->setHtmlIdPrefix('Invoice_');
        $form->setFieldNameSuffix('Invoice');

        $fieldset = $form->addFieldset(
            'base_fieldset',
            ['legend' => __('General')]
        );

        if ($model->getId()) {
            $fieldset->addField(
                'id',
                'hidden',
                ['name' => 'id',
                 'value' =>  $orderCustomsInvoiceData['id']  ]
            );
        }
        
       $fieldset->addField(
            'orderid' ,
            'hidden',
            [
                'name'     =>  'order_id',
                'label'    => __( "Order ID" ),
                'value'    =>  $orderCustomsInvoiceData['order_id'],
                'class'    =>  "orderid_class",
              //  'disabled' => "disabled",
            ]
        );

            
        // $obarr = []; 'shipping_method'
        $availableKeys= ['shipping_description',
                         //'base_grand_total',
                         //'base_subtotal',
                         'grand_total',
                         'subtotal',
                         //'total_qty_ordered',
                         //'total_due',
                         //'subtotal_incl_tax',
                        // 'base_total_due',
                         'base_shipping_amount',
                         'increment_id',
                         'created_at',
                         ];
        foreach ($keys  as $value) {
          if( !in_array( $value, $availableKeys )){ continue; }
          $val   = $value;

          if( gettype (  $tmp[ $val ]  ) == 'string'){
              if($val == 'subtotal'){
                $fieldset->addField(
                    $val.'-label',
                    'label',
                    [
                        'name'     => $val.'-label',
                        'label'    => __( $val ),
                        'value'    => $orderCustomsInvoiceData[$val],
//                       'disabled'  => true
                    ]
                );
                $fieldset->addField(
                    $val ,
                    'hidden',
                    [
                        'name'     => $val ,
                        'label'    => __( $val ),
                        'class'    => $val."_action_class",
                        'value'    => $orderCustomsInvoiceData[$val],
//                       'disabled'  => true
                    ]
                );
              }elseif($val == 'grand_total'){
                $fieldset->addField(
                    $val.'-label'  ,
                    'label',
                    [
                        'name'     => $val.'-label' ,
                        'label'    => __( $val ),
                        'value'    => $orderCustomsInvoiceData[$val],
//                       'disabled'  => true
                    ]
                ); 
                $fieldset->addField(
                    $val ,
                    'hidden',
                    [
                        'name'     => $val ,
                        'label'    => __( $val ),
                        'class'    => $val."_action_class",
                        'value'    => $orderCustomsInvoiceData[$val],
//                       'disabled'  => true
                    ]
                ); 
              }elseif($val == 'base_shipping_amount'){
                $fieldset->addField(
                    $val ,
                    'text',
                    [
                        'name'     => $val ,
                        'label'    => __( $val ),
                        'class'    => $val."_action_class",
                        'value'    => $orderCustomsInvoiceData[$val],
                      // 'required'     => true
                    ]
                ); 
              }else{
                $fieldset->addField(
                    $val ,
                    'text',
                    [
                        'name'     => $val ,
                        'label'    => __( $val ),
                        'class'    => $val."_action_class",
                        'value'    => $orderCustomsInvoiceData[ $val ]
                      // 'required'     => true
                    ]
                ); 
              }
              
          }else{
            
          }
        }





        $data = $model->getData();
//        $form->setValues($data);

        $payment = $model->getPayment();
        $method = $payment->getMethodInstance();
        $methodTitle = $method->getTitle();
        $fieldset->addField(
            'payment_method' ,
            'text',
            [
                'name'     =>  'payment_method',
                'label'    => __( "Payment Method" ),
                'value'    => $orderCustomsInvoiceData['payment_method']
            ]
        );


        $shippingAddress = $model -> getShippingAddress();
        $fieldset->addField(
            'shippingaddress' ,
            'textarea',
            [
                'name'     =>  'shippingaddress',
                'label'    => __( "Shipping Address" ),
                'value'    => $orderCustomsInvoiceData['shippingaddress']
            ]
        );

        $billingAddress = $model -> getBillingAddress();
        $fieldset->addField(
            'billingaddress' ,
            'textarea',
            [
                'name'     =>  'billingaddress',
                'label'    => __( "Billing Address" ),
                'value'    => $orderCustomsInvoiceData['billingaddress']
            ]
        );

        $fieldset->addField(
            'discountlabel' ,
            'text',
            [
                'name'     =>  'discountlabel',
                'label'    => __( "Discount Label" ),
                'required' => false,
                'value'    => $orderCustomsInvoiceData['discountlabel'],
                'class'    =>  "discountlabel_action_class",
            ]
        );

        $fieldset->addField(
            'discountamount' ,
            'text',
            [
                'name'     =>  'discountamount',
                'label'    => __( "Discount Amount" ),
                'required' => false,
                'class'    =>  "discountamount_action_class",
            ]
        );

        $fieldset->addField(
            'thankyoumessage' ,
            'text',
            [
                'name'     =>  'thankyoumessage',
                'label'    => __( "Thank You Message" ),
                'required' => false,
                'value'    => $orderCustomsInvoiceData['thankyoumessage'],
                'class'    =>  "thankyoumessage_action_class",
            ]
        );

        $fieldset->addField(
            'invoicefooternotice' ,
            'text',
            [
                'name'     =>  'invoicefooternotice',
                'label'    => __( "Footer notice" ),
                'required' => false,
                'value'    => $orderCustomsInvoiceData['invoicefooternotice'],
                'class'    =>  "invoicefooternotice_action_class",
            ]
        );
        
        $fieldset->addField(
            'currencysymbol' ,
            'text',
            [
                'name'     =>  'currencysymbol',
                'label'    => __( "Currency symbol" ),
                'required' => false,
                'value'    => $orderCustomsInvoiceData['currencysymbol'],
                'class'    =>  "currencysymbol_action_class",
                'note'     =>  'that # specified the position of the amount'
            ]
        );


        $this->setForm($form);
    }
    
    private function prepareNewForm( $model ){
               /** @var $model \NovaMinds\CustomsInvoice\Model\Invoice */
        // $model = $this->_coreRegistry->registry('customs_invoice');
         $tmp = $model->getData();
         $keys =  array_keys( $tmp );
         // print_r( $keys );
         // die;
        /** @var \Magento\Framework\Data\Form $form */
        $subtotal = 0;
        $grandtotal = 0;
        
        // $customsInvoice = $this->customsInvoiceRepo->get( $model->getId() );
        
        $priceReductionvalue = $this->scopeConfig->getValue(
                                    'customsinvoice/customsinvoicedata/precent',
                                    \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        // var_dump($priceReductionvalue);die;
        $priceReductionPrecent = 1;
        if( !empty( $priceReductionvalue ) ){
            $tmpval = intval($priceReductionvalue);
            if( $tmpval > 0 ){
                $priceReductionPrecent = $tmpval/100;
            }
        }
        
        foreach ( $tmp[ 'items' ] as $item ) {
            $product = $item->getProduct();
            $prod = $this->_productRepository->getById($product->getId(), false, 0);
            $customsPrice= $prod->getData ('customs_price');
            $productPrice = 0 ;
            if (empty($customsPrice)) {
              $tmpProductPrice = $prod->getPrice();
              if( $priceReductionPrecent < 1){
                $productPrice =  $tmpProductPrice - ( $tmpProductPrice  * $priceReductionPrecent );
              }else { // no entry or 100% and up 200%
                $productPrice =  $tmpProductPrice;
              }
            }else{
              $productPrice = $customsPrice;
            }
            $quantity = $item->getQtyOrdered();
            $subtotal += $productPrice * $quantity;
         }
         $subtotal = number_format((float)$subtotal , 2, '.', '');
         $grandtotal = number_format((float)$subtotal + $tmp['base_shipping_amount'], 2, '.', '');
        
        
        $form = $this->_formFactory->create();
        $form->setHtmlIdPrefix('Invoice_');
        $form->setFieldNameSuffix('Invoice');

        $fieldset = $form->addFieldset(
            'base_fieldset',
            ['legend' => __('General')]
        );

        if ($model->getId()) {
            $fieldset->addField(
                'id',
                'hidden',
                ['name' => 'id']
            );
        }
        
       $fieldset->addField(
            'orderid' ,
            'hidden',
            [
                'name'     =>  'order_id',
                'label'    => __( "Order ID" ),
                'value'    =>  $model -> getId(),
                'class'    =>  "orderid_class",
              //  'disabled' => "disabled",
            ]
        );

            
        // $obarr = []; 'shipping_method'
        $availableKeys= ['shipping_description',
                         //'base_grand_total',
                         //'base_subtotal',
                         'grand_total',
                         'subtotal',
                         //'total_qty_ordered',
                         //'total_due',
                         //'subtotal_incl_tax',
                        // 'base_total_due',
                         'base_shipping_amount',
                         'increment_id',
                         'created_at',
                         ];
        foreach ($keys  as $value) {
          if( !in_array( $value, $availableKeys )){ continue; }
          $val   = $value;

          if( gettype (  $tmp[ $val ]  ) == 'string'){
              if($val == 'subtotal'){
                $fieldset->addField(
                    $val.'-label',
                    'label',
                    [
                        'name'     => $val.'-label',
                        'label'    => __( $val ),
                        'value'    => $subtotal,
//                       'disabled'  => true
                    ]
                );
                $fieldset->addField(
                    $val ,
                    'hidden',
                    [
                        'name'     => $val ,
                        'label'    => __( $val ),
                        'class'    => $val."_action_class",
                        'value'    => $subtotal,
//                       'disabled'  => true
                    ]
                );
              }elseif($val == 'grand_total'){
                $fieldset->addField(
                    $val.'-label'  ,
                    'label',
                    [
                        'name'     => $val.'-label' ,
                        'label'    => __( $val ),
                        'value'    => $grandtotal,
//                       'disabled'  => true
                    ]
                ); 
                $fieldset->addField(
                    $val ,
                    'hidden',
                    [
                        'name'     => $val ,
                        'label'    => __( $val ),
                        'class'    => $val."_action_class",
                        'value'    => $grandtotal,
//                       'disabled'  => true
                    ]
                ); 
              }elseif($val == 'base_shipping_amount'){
                $fieldset->addField(
                    $val ,
                    'text',
                    [
                        'name'     => $val ,
                        'label'    => __( $val ),
                        'class'    => $val."_action_class",
                        'value'    => number_format((float)$tmp[ $val ], 2, '.', '')
                      // 'required'     => true
                    ]
                ); 
              }else{
                $fieldset->addField(
                    $val ,
                    'text',
                    [
                        'name'     => $val ,
                        'label'    => __( $val ),
                        'class'    => $val."_action_class",
                        'value'    => $tmp[ $val ]
                      // 'required'     => true
                    ]
                ); 
              }
              
          }else{
            
          }
        }





        $data = $model->getData();
//        $form->setValues($data);

        $payment = $model->getPayment();
        $method = $payment->getMethodInstance();
        $methodTitle = $method->getTitle();
        $fieldset->addField(
            'payment_method' ,
            'text',
            [
                'name'     =>  'payment_method',
                'label'    => __( "Payment Method" ),
                'value'    => $methodTitle
            ]
        );


        $shippingAddress = $model -> getShippingAddress();
        $fieldset->addField(
            'shippingaddress' ,
            'textarea',
            [
                'name'     =>  'shippingaddress',
                'label'    => __( "Shipping Address" ),
                'value'    => $this->render->format( $shippingAddress , "text")
            ]
        );

        $billingAddress = $model -> getBillingAddress();
        $fieldset->addField(
            'billingaddress' ,
            'textarea',
            [
                'name'     =>  'billingaddress',
                'label'    => __( "Billing Address" ),
                'value'    => $this->render->format( $billingAddress , "text")
            ]
        );

        $fieldset->addField(
            'discountlabel' ,
            'text',
            [
                'name'     =>  'discountlabel',
                'label'    => __( "Discount Label" ),
                'required' => false,
                'value'    => __( "Discount" ),
                'class'    =>  "discountlabel_action_class",
            ]
        );

        $fieldset->addField(
            'discountamount' ,
            'text',
            [
                'name'     =>  'discountamount',
                'label'    => __( "Discount Amount" ),
                'required' => false,
                'class'    =>  "discountamount_action_class",
            ]
        );

        $fieldset->addField(
            'thankyoumessage' ,
            'text',
            [
                'name'     =>  'thankyoumessage',
                'label'    => __( "Thank You Message" ),
                'required' => false,
                'value'    => __( "Thank you for your business!" ),
                'class'    =>  "thankyoumessage_action_class",
            ]
        );

        $fieldset->addField(
            'invoicefooternotice' ,
            'text',
            [
                'name'     =>  'invoicefooternotice',
                'label'    => __( "Footer notice" ),
                'required' => false,
                'value'    => __( "HerbsGate.com - Ruislip, UK | +44 (0) 77527 33633 | support@herbsgate.com" ),
                'class'    =>  "invoicefooternotice_action_class",
            ]
        );
        
        $fieldset->addField(
            'currencysymbol' ,
            'text',
            [
                'name'     =>  'currencysymbol',
                'label'    => __( "Currency symbol" ),
                'required' => false,
                'value'    => __( "£#" ),
                'class'    =>  "currencysymbol_action_class",
                'note'     =>  'that # specified the position of the amount'
            ]
        );


        $this->setForm($form);
    }
    /**
     * Prepare label for tab
     *
     * @return string
     */
    public function getTabLabel()
    {
        return __('Invoice Info');
    }

    /**
     * Prepare title for tab
     *
     * @return string
     */
    public function getTabTitle()
    {
        return __('Invoice Info');
    }

    /**
     * {@inheritdoc}
     */
    public function canShowTab()
    {
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function isHidden()
    {
        return false;
    }
}
