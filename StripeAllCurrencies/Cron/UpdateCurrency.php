<?php

namespace NovaMinds\StripeAllCurrencies\Cron;

use NovaMinds\StripeAllCurrencies\Helper\Data;
use NovaMinds\StripeAllCurrencies\Model\CurrencyServiceFactory;
use NovaMinds\StripeAllCurrencies\Model\RatesFactory;
use Psr\Log\LoggerInterface;

class UpdateCurrency
{
    /** @var \NovaMinds\StripeAllCurrencies\Helper\Data $helper */
    protected $helper;

    /** @var \NovaMinds\StripeAllCurrencies\Model\CurrencyServiceFactory $currencyServiceFactory */
    protected $currencyServiceFactory;

    /** @var \NovaMinds\StripeAllCurrencies\Model\RatesFactory $ratesFactory */
    protected $ratesFactory;

    /** @var \Psr\Log\LoggerInterface $logger */
    protected $logger;

    /**
     * ConfigPlugin constructor.
     *
     * @param \NovaMinds\StripeAllCurrencies\Helper\Data                  $helper
     * @param \NovaMinds\StripeAllCurrencies\Model\CurrencyServiceFactory $currencyServiceFactory
     * @param \NovaMinds\StripeAllCurrencies\Model\RatesFactory           $ratesFactory
     * @param \Psr\Log\LoggerInterface                                   $logger
     */
    public function __construct(
        Data $helper,
        CurrencyServiceFactory $currencyServiceFactory,
        RatesFactory $ratesFactory,
        LoggerInterface $logger
    ) {
        $this->helper = $helper;
        $this->currencyServiceFactory = $currencyServiceFactory;
        $this->ratesFactory = $ratesFactory;
        $this->logger = $logger;
    }

    /**
     * @return $this
     */
    public function execute()
    {
        
        
        /** @var \NovaMinds\StripeAllCurrencies\Model\CurrencyService\CurrencyServiceInterface $currencyService */
        $currencyService = $this->currencyServiceFactory->load($this->helper->getCurrencyServiceId());

        /** @var \NovaMinds\StripeAllCurrencies\Model\Rates $ratesModel */
        $ratesModel = $this->ratesFactory->create();
        $ratesModel->updateRateFromService($currencyService);
        $ratesModel->save();
        $this->logger->info('Rate is updated');

        return $this;
    }
}
