<?php

namespace NovaMinds\StripeAllCurrencies\Preference\Payment\Model\Cart\SalesModel;

use NovaMinds\StripeAllCurrencies\Helper\Data;
use NovaMinds\StripeAllCurrencies\Model\CurrencyServiceFactory;

/**
 * @method float setStripeSubtotal(float $amt)
 * @method float setStripeGrandTotal(float $amt)
 * @method float setStripeShippingAmount(float $amt)
 * @method float setStripeDiscountAmount(float $amt)
 * @method float setStripeTaxAmount(float $amt)
 * @method float setStripeRate(float $amt)
 * @method string setStripeCurrencyCode(string $code)
 * Wrapper for \Magento\Quote\Model\Quote sales model
 */
class Quote extends \Magento\Payment\Model\Cart\SalesModel\Quote implements SalesModelInterface
{
    /** @var \NovaMinds\StripeAllCurrencies\Helper\Data $helper */
    protected $helper;

    /** @var \NovaMinds\StripeAllCurrencies\Model\CurrencyServiceFactory $currencyServiceFactory */
    protected $currencyServiceFactory;

    /** @var \NovaMinds\StripeAllCurrencies\Model\CurrencyService\CurrencyServiceInterface|null $currencyService */
    protected $currencyService = null;

    /**
     * @param \Magento\Quote\Model\Quote                                 $quoteModel
     * @param \NovaMinds\StripeAllCurrencies\Helper\Data                  $helper
     * @param \NovaMinds\StripeAllCurrencies\Model\CurrencyServiceFactory $currencyServiceFactory
     */
    public function __construct(
        \Magento\Quote\Model\Quote $quoteModel,
        Data $helper,
        CurrencyServiceFactory $currencyServiceFactory
    ) {
        parent::__construct($quoteModel);
        $this->helper = $helper;
        $this->currencyServiceFactory = $currencyServiceFactory;
    }

    /**
     * {@inheritdoc}
     * used in Magento\Stripe\Model\Cart, function _validate()
     */
    public function getDataUsingMethod($key, $args = null)
    {
        if ($key == 'base_grand_total') {
            return $this->getCurrencyService()->exchange(parent::getDataUsingMethod($key, $args));
        }

        return parent::getDataUsingMethod($key, $args);
    }

    /**
     * @return false|null|\NovaMinds\StripeAllCurrencies\Model\CurrencyService\CurrencyServiceInterface
     */
    public function getCurrencyService()
    {
        if (!$this->currencyService) {
            $this->currencyService = $this->currencyServiceFactory->load($this->helper->getCurrencyServiceId());
        }

        return $this->currencyService;
    }

    /**
     * @return float
     */
    public function getStripeSubtotal()
    {
        return $this->_salesModel->getStripeSubtotal();
    }

    /**
     * @return float
     */
    public function getStripeGrandTotal()
    {
        return $this->_salesModel->getStripeGrandTotal();
    }

    /**
     * @return float
     */
    public function getStripeShippingAmount()
    {
        return $this->_salesModel->getStripeShippingAmount();
    }

    /**
     * @return float
     */
    public function getStripeDiscountAmount()
    {
        return $this->_salesModel->getStripeDiscountAmount();
    }

    /**
     * @return float
     */
    public function getStripeTaxAmount()
    {
        return $this->_salesModel->getStripeTaxAmount();
    }

    /**
     * @return float
     */
    public function getStripeRate()
    {
        return $this->_salesModel->getStripeRate();
    }

    /**
     * @return string
     */
    public function getStripeCurrencyCode()
    {
        return $this->_salesModel->getStripeCurrencyCode();
    }
}
