<?php

namespace NovaMinds\StripeAllCurrencies\Preference\Payment\Model\Cart\SalesModel;

/**
 * Interface SalesModelInterface
 *
 * @package NovaMinds\StripeAllCurrencies\Model\Preference\Payment\Cart\SalesModel
 */
interface SalesModelInterface
{
    /**
     * @return float
     */
    public function getStripeSubtotal();

    /**
     * @return float
     */
    public function getStripeGrandTotal();

    /**
     * @return float
     */
    public function getStripeShippingAmount();

    /**
     * @return float
     */
    public function getStripeDiscountAmount();

    /**
     * @return float
     */
    public function getStripeTaxAmount();

    /**
     * @return float
     */
    public function getStripeRate();

    /**
     * @return string
     */
    public function getStripeCurrencyCode();
}
