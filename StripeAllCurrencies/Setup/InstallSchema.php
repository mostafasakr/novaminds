<?php

namespace NovaMinds\StripeAllCurrencies\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

/**
 * Class InstallSchema
 *
 * @package NovaMinds\StripeAllCurrencies\Setup
 */
class InstallSchema implements InstallSchemaInterface
{
    /**
     * @param \Magento\Framework\Setup\SchemaSetupInterface   $setup
     * @param \Magento\Framework\Setup\ModuleContextInterface $context
     */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;

        $installer->startSetup();

        $itemTables = [
            $installer->getTable('quote_item'),
            $installer->getTable('sales_order_item'),
            $installer->getTable('sales_invoice_item'),
            $installer->getTable('sales_creditmemo_item'),
        ];

        $orderTables = [
            $installer->getTable('quote'),
            $installer->getTable('sales_order'),
            $installer->getTable('sales_invoice'),
            $installer->getTable('sales_creditmemo'),
        ];

        $connection = $installer->getConnection();

        foreach ($itemTables as $itemTable) {
            $connection->addColumn(
                $itemTable, 'stripe_price', $this->getAmountConfig('Price in stripe currency')
            );

            $connection->addColumn(
                $itemTable, 'stripe_row_total', $this->getAmountConfig('Row total in stripe currency')
            );
        }

        foreach ($orderTables as $orderTable) {
            $connection->addColumn(
                $orderTable, 'stripe_subtotal', $this->getAmountConfig('Subtotal in stripe currency')
            );
            $connection->addColumn(
                $orderTable, 'stripe_grand_total', $this->getAmountConfig('Grand total in stripe currency')
            );
            $connection->addColumn(
                $orderTable, 'stripe_tax_amount', $this->getAmountConfig('Tax in stripe currency')
            );
            $connection->addColumn(
                $orderTable, 'stripe_shipping_amount', $this->getAmountConfig('Shipping in stripe currency')
            );
            $connection->addColumn(
                $orderTable, 'stripe_discount_amount', $this->getAmountConfig('Discount in stripe currency')
            );
            $connection->addColumn(
                $orderTable, 'stripe_rate', $this->getAmountConfig('Stripe rate')
            );
            $connection->addColumn(
                $orderTable, 'stripe_currency_code', [
                    'type'     => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'nullable' => true,
                    'length'   => 20,
                    'comment'  => 'Stripe currency code'
                ]
            );
        }

        $installer->endSetup();
    }

    /**
     * @param $comment
     * @return array
     */
    public function getAmountConfig($comment)
    {
        return [
            'type'     => \Magento\Framework\DB\Ddl\Table::TYPE_DECIMAL,
            'nullable' => true,
            'length'   => '12,4',
            'comment'  => $comment,
            'default'  => '0'
        ];
    }
}
