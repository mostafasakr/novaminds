<?php

namespace NovaMinds\StripeAllCurrencies\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

/**
 * Class Rates
 *
 * @package NovaMinds\StripeAllCurrencies\Model\ResourceModel
 */
class Rates extends AbstractDb

{
    /**
     * Define main table
     */
    protected function _construct()
    {
        $this->_init('novaminds_stripeallcurrencies_rates', 'entity_id');
    }
}
