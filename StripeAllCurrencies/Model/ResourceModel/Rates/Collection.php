<?php

namespace NovaMinds\StripeAllCurrencies\Model\ResourceModel\Rates;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

/**
 * Class Collection
 *
 * @package NovaMinds\StripeAllCurrencies\Model\ResourceModel\Rates
 */
class Collection extends AbstractCollection
{
    /**
     * Define model & resource model
     */
    protected function _construct()
    {
        $this->_init(
            'NovaMinds\StripeAllCurrencies\Model\Rates',
            'NovaMinds\StripeAllCurrencies\Model\ResourceModel\Rates'
        );
    }
}

