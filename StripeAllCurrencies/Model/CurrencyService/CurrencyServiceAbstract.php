<?php

namespace NovaMinds\StripeAllCurrencies\Model\CurrencyService;

use \Magento\Framework\HTTP\Client\Curl;
use \Magento\Store\Model\StoreManagerInterface;
use Psr\Log\LoggerInterface;
use \NovaMinds\StripeAllCurrencies\Helper\Data;
use \NovaMinds\StripeAllCurrencies\Model\RatesFactory;

/**
 * Class CurrencyServiceAbstract
 *
 * @package NovaMinds\StripeAllCurrencies\Model\CurrencyService
 */
abstract class CurrencyServiceAbstract
{
    /** @var \Magento\Framework\HTTP\Client\Curl $_curl */
    protected $curl;

    /** @var \Magento\Store\Model\StoreManagerInterface $storeManager */
    protected $storeManager;

    /** @var \NovaMinds\StripeAllCurrencies\Helper\Data $helper */
    protected $helper;

    /** @var \NovaMinds\StripeAllCurrencies\Model\RatesFactory $ratesFactory */
    protected $ratesFactory;

    /** @var  string $stripeCurrencyCode */
    protected $stripeCurrencyCode = null;

    /** @var  int $_serviceId */
    protected $serviceId;

    /** @var \Psr\Log\LoggerInterface $logger */
    protected $logger;
    
    /**
 * @var \Magento\Framework\App\State
 */
protected $state;
protected $request;
protected $_currentWebsite;

    /**
     * CurrencyServiceAbstract constructor.
     *
     * @param \Magento\Framework\HTTP\Client\Curl              $curl
     * @param \Magento\Store\Model\StoreManagerInterface       $storeManager
     * @param \NovaMinds\StripeAllCurrencies\Helper\Data        $helper
     * @param \NovaMinds\StripeAllCurrencies\Model\RatesFactory $ratesFactory
     * @param \Psr\Log\LoggerInterface                         $logger
     * @param int                                              $serviceId
     */
    public function __construct(
        Curl $curl,
        StoreManagerInterface $storeManager,
        Data $helper,
        RatesFactory $ratesFactory,
        \Magento\Framework\App\RequestInterface $request,
        \Magento\Framework\App\State $state,
        LoggerInterface $logger,
        $serviceId
    ) {
        $this->curl = $curl;
        $this->storeManager = $storeManager;
        $this->helper = $helper;
        $this->ratesFactory = $ratesFactory;
        $this->logger = $logger;
        $this->state = $state;
        $this->request =  $request;
        $this->serviceId = $serviceId;
    }

    /**
     * @return \Magento\Framework\HTTP\Client\Curl
     */
    public function getCurl()
    {
        return $this->curl;
    }

    /**
     * @return int
     */
    public function getServiceId() {


        return $this->serviceId;
    }

    /**
     * @return string
     */
    public function getStoreCurrencyCode($storeId = NULL) {
        if (is_null($storeId)) {
            $websiteId = $this->resolveCurrentWebsiteId();
            if (is_numeric($websiteId)) {
                return $this->storeManager->getWebsite($websiteId)->getBaseCurrency()->getCode();
            }
        }
       
        
            return $this->storeManager->getStore($storeId)->getCurrentCurrency()->getCode();
       
            
    }

    public function resolveCurrentWebsiteId()
{
        if ($this->state->getAreaCode() == \Magento\Framework\App\Area::AREA_ADMINHTML ) {
      
                // in admin area
                /** @var \Magento\Framework\App\RequestInterface $request */
                $request = $this->request;
                $this->_currentWebsite = (int) $request->getParam('website', 0);
            } else {
                // frontend area
                $this->_currentWebsite = true; // get current store from the store resolver
            
        }


        return $this->_currentWebsite;
}

    /**
     * @return int
     */
    public function getStoreId($storeId = null) {
        if(is_null($storeId)){
          
        $websiteId = $this->resolveCurrentWebsiteId();
         
        if (is_numeric($websiteId)) {
            return $this->storeManager->getWebsite($websiteId)
                            ->getDefaultGroup()
                            ->getDefaultStoreId();
        }
        }
        return $this->storeManager->getStore($storeId)->getStoreId();
    }

    /**
     * @return \NovaMinds\StripeAllCurrencies\Helper\Data
     */
    public function getHelper()
    {
        return $this->helper;
    }

    /**
     * @return string
     */
    public function getStripeCurrencyCode($storeId =null) {
  

        if (!$this->stripeCurrencyCode) {
            $this->stripeCurrencyCode = $this->getHelper()->getStripeCurrency($this->getStoreId($storeId));
        }


        return $this->stripeCurrencyCode;
    }

    /**
     * @param string $stripeCurrencyCode
     * @return string
     */
    public function setStripeCurrencyCode(string $stripeCurrencyCode)
    {
        return $this->stripeCurrencyCode = $stripeCurrencyCode;
    }

    /**
     * Exchange rates from database
     *
     * @param float $amt
     * @param int   $precision
     * @return float
     */
    public function exchange($amt, $precision = 4, $storeId = null) {
        $exchanged = 0;

        try {
            /** @var \NovaMinds\StripeAllCurrencies\Model\Rates $ratesModel */
            $ratesModel = $this->ratesFactory->create();
            $rate = $ratesModel->getExistingRate(
                    $this->getServiceId(), $this->getStoreCurrencyCode($storeId), $this->getStripeCurrencyCode($storeId)
            );


            $exchanged = round($amt * $rate->getRate(), $precision);
        } catch (\Exception $e) {

            $logger->info($e->getMessage());
        }



        return $exchanged;
    }

}
