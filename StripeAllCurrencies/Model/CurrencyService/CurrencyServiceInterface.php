<?php

namespace NovaMinds\StripeAllCurrencies\Model\CurrencyService;

/**
 * Interface CurrencyService
 *
 * @package NovaMinds\StripeAllCurrencies\Model\CurrencyService
 */
interface CurrencyServiceInterface
{
    /**
     * @return string
     */
    public function getApiUrl();

    /**
     * Exchange rates from api service
     *
     * @param float $amt
     *
     * @return float
     */
    public function exchangeFromService(float $amt);

    /**
     * Exchange rates from database
     *
     * @param float $amt
     * @param int   $precision
     *
     * @return float
     */
    public function exchange($amt, $precision = 4);

    /**
     * @return string
     */
    public function getStoreCurrencyCode();

    /**
     * @param null|int $storeId
     * @return string
     */
    public function getStripeCurrencyCode();

    /**
     * @param string $stripeCurrencyCode
     * @return string
     */
    public function setStripeCurrencyCode(string $stripeCurrencyCode);
}
