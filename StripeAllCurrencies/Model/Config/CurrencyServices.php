<?php

namespace NovaMinds\StripeAllCurrencies\Model\Config;

use Magento\Framework\Option\ArrayInterface;
use NovaMinds\StripeAllCurrencies\Model\CurrencyServiceFactory;

/**
 * Class ConverterServices
 *
 * @package NovaMinds\StripeAllCurrencies\Model\Config
 */
class CurrencyServices implements ArrayInterface
{
    protected $_services;

    /**
     * CurrencyServices constructor.
     *
     * @param \NovaMinds\StripeAllCurrencies\Model\CurrencyServiceFactory $serviceFactory
     */
    public function __construct(CurrencyServiceFactory $serviceFactory)
    {
        $this->_services = $serviceFactory->getServices();
    }

    /**
     * @return array
     */
    public function toOptionArray()
    {
        $options = [];
        foreach ($this->_services as $key => $service) {
            $options[] = ['value' => $key, 'label' => $service['label']];
        }

        return $options;
    }
}
