<?php namespace NovaMinds\StripeAllCurrencies\Model;
class AdditionalConfigProvider implements \Magento\Checkout\Model\ConfigProviderInterface
{
    private $data;
    
    private $storeManager;
    public function __construct(\NovaMinds\StripeAllCurrencies\Helper\Data $data,\Magento\Store\Model\StoreManagerInterface $storeManager) {
        $this->data=$data;
        
        $this->storeManager=$storeManager;
    }
   public function getConfig()
   {   
        $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/nada/test.log');
        $logger = new \Zend\Log\Logger();
        $logger->addWriter($writer);
//        $ratesCollection= $this->collectionFactory->create();
//        $ratesCollection->addAttributeToSelect('*')
//                        ->getData();
//        $rates = $this->currencyFactory->load($this->getCurrencyServiceId())->getData();
        $logger->info($this->data->getCurrencyRates());
        $output['rates']= $this->data->getCurrencyRates();
        $output['strip_message'] = $this->data->getCurrencyRates();
        return $output;
   }
}