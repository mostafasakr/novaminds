<?php

namespace NovaMinds\StripeAllCurrencies\Model;

use Magento\Framework\Data\Collection\AbstractDb;
use Magento\Framework\Model\AbstractModel;
use Magento\Framework\Model\Context;
use Magento\Framework\Model\ResourceModel\AbstractResource;
use Magento\Framework\Registry;
use Magento\Framework\Stdlib\DateTime\DateTime;
use NovaMinds\StripeAllCurrencies\Model\CurrencyService\CurrencyServiceInterface;

/**
 * Class Rates
 *
 * @method int getEntityId()
 * @method int getServiceId()
 * @method int setServiceId($serviceId)
 * @method string getBaseCurrencyCode()
 * @method string setBaseCurrencyCode($baseCurrencyCode)
 * @method string getStripeCurrencyCode()
 * @method string setStripeCurrencyCode($stripeCurrencyCode)
 * @method float getRate()
 * @method float setRate($rate)
 * @method string getUpdatedAt()
 * @method string setUpdatedAt($gmtDate)
 * @package NovaMinds\StripeAllCurrencies\Model
 */
class Rates extends AbstractModel
{
    /** @var \Magento\Framework\Stdlib\DateTime\DateTime|null $dateTime */
    protected $dateTime = null;

    /**
     * @param \Magento\Framework\Stdlib\DateTime\DateTime             $dateTime
     * @param \Magento\Framework\Model\Context                        $context
     * @param \Magento\Framework\Registry                             $registry
     * @param \Magento\Framework\Model\ResourceModel\AbstractResource $resource
     * @param \Magento\Framework\Data\Collection\AbstractDb           $resourceCollection
     * @param array                                                   $data
     */
    public function __construct(
        DateTime $dateTime,
        Context $context,
        Registry $registry,
        AbstractResource $resource = null,
        AbstractDb $resourceCollection = null,
        array $data = []
    ) {
        $this->dateTime = $dateTime;
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
    }

    protected function _construct()
    {
        $this->_init('NovaMinds\StripeAllCurrencies\Model\ResourceModel\Rates');
    }

    /**
     * @param \NovaMinds\StripeAllCurrencies\Model\CurrencyService\CurrencyServiceInterface $currencyService
     * @return $this
     */
    public function updateRateFromService(CurrencyServiceInterface $currencyService)
    {
        $baseCurrencyCode = $currencyService->getStoreCurrencyCode();
        $stripeCurrencyCode = $currencyService->getStripeCurrencyCode();
        $rate = $currencyService->exchangeFromService(1);
        $serviceId = $currencyService->getServiceId();

        /** @var \NovaMinds\StripeAllCurrencies\Model\Rates $rateItem */
        $rateItem = $this->getExistingRate($serviceId, $baseCurrencyCode, $stripeCurrencyCode);

        if (!$rateItem->getEntityId()) {
            $this->insertRate($serviceId, $baseCurrencyCode, $stripeCurrencyCode, $rate);
        } else {
            $rateItem->setRate($this->roundRate($rate));
            $rateItem->setUpdatedAt($this->dateTime->gmtDate());
            $rateItem->save();
        }

        return $this;
    }

    /**
     * @param int    $serviceId
     * @param string $baseCurrencyCode
     * @param string $stripeCurrencyCode
     * @param float  $rate
     * @return $this
     */
    public function insertRate($serviceId, $baseCurrencyCode, $stripeCurrencyCode, $rate)
    {
        $data = [
            'service_id'           => $serviceId,
            'base_currency_code'   => $baseCurrencyCode,
            'stripe_currency_code' => $stripeCurrencyCode,
            'rate'                 => $this->roundRate($rate),
            'updated_at'           => $this->dateTime->gmtDate()
        ];
        $this->setData($data);

        return $this;
    }

    /**
     * @param int    $serviceId
     * @param string $baseCurrency
     * @param string $stripeCurrency
     * @return $this|null|\Magento\Framework\DataObject
     */
    public function getExistingRate($serviceId, $baseCurrency, $stripeCurrency)
    {
        return $this->getCollection()
                    ->addFieldToFilter('service_id', $serviceId)
                    ->addFieldToFilter('base_currency_code', $baseCurrency)
                    ->addFieldToFilter('stripe_currency_code', $stripeCurrency)
                    ->getFirstItem();
    }

    /**
     * @param $rate
     * @return float
     */
    public function roundRate($rate)
    {
        return round($rate, 4);
    }
}
