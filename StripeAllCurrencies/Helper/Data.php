<?php

namespace NovaMinds\StripeAllCurrencies\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Sales\Api\Data\OrderInterface;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\ObjectManagerInterface;
use Magento\Framework\App\Helper\Context;
use Magento\Store\Model\ScopeInterface;

/**
 * Class Data
 *
 * @package NovaMinds\StripeAllCurrencies\Helper
 */
class Data extends AbstractHelper {

    protected $storeManager;
    protected $objectManager;

    const XML_PATH_GENERAL = 'novaminds_stripeallcurrencies/general/';

    protected $state;
    protected $request;
    protected $_currentWebsite;
    
    protected $collectionFactory;

    /**
     * Data constructor.
     *
     * @param \Magento\Framework\App\Helper\Context      $context
     * @param \Magento\Framework\ObjectManagerInterface  $objectManager
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     */
    public function __construct(
    Context $context, ObjectManagerInterface $objectManager, \Magento\Framework\App\RequestInterface $request, \Magento\Framework\App\State $state, StoreManagerInterface $storeManager
    , \NovaMinds\StripeAllCurrencies\Model\ResourceModel\Rates\CollectionFactory $collectionFactory) {
        parent::__construct($context);
        $this->objectManager = $objectManager;
        $this->storeManager = $storeManager;
        $this->state = $state;
        $this->request = $request;
        $this->collectionFactory=$collectionFactory;
    }

    /**
     * @param string   $field
     * @param null|int $storeId
     * @return mixed
     */
    public function getConfigValue($field, $storeId = null) {
        return $this->scopeConfig->getValue(
                        $field, ScopeInterface::SCOPE_STORE, $storeId
        );
    }

    /**
     * @param string   $code
     * @param null|int $storeId
     * @return mixed
     */
    public function getGeneralConfig($code, $storeId = null) {
        if (!$storeId) {
            $storeId = $this->getStoreId();
        }
        return $this->getConfigValue(self::XML_PATH_GENERAL . $code, $storeId);
    }

    /**
     * @return bool
     */
    public function isModuleEnabled($storeId = null) {
        return parent::isModuleOutputEnabled('NovaMinds_StripeAllCurrencies') && $this->getGeneralConfig('enabled', $storeId);
    }

    /**
     * @param null|int $storeId
     * @return bool
     */
    public function getCurrencyServiceId($storeId = null) {
        return $this->getGeneralConfig('currencyservice', $storeId);
    }

    /**
     * @param null|int $storeId
     * @return string
     */
    public function getStripeCurrency($storeId = null) {
        return $this->getGeneralConfig('stripecurrency', $storeId);
    }

    public function getConversionMessage($storeId = null) {
        return $this->getGeneralConfig('conversion_message', $storeId);
//        return $this->scopeConfig->getValue('novaminds_stripeallcurrencies/options/conversion_message', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    /**
     * @return array
     */
    public function getStripePaymentMethods() {
        return [
            'stripealipay',
            'stripebancontact',
            'stripecore',
            'stripecreditcards',
            'stripegiropay',
            'stripeideal',
            'stripeprzelewy',
            'stripesepa',
            'stripecreditcards_vault',
            'stripesofort'
        ];
    }

    /**
     * @param \Magento\Sales\Api\Data\OrderInterface $order
     * @return bool
     */
    public function isOrderPlacedByStripe(OrderInterface $order) {
        return in_array($order->getPayment()->getMethod(), $this->getStripePaymentMethods());
    }

    public function resolveCurrentWebsiteId() {
        if (!$this->_currentWebsite) {
            if ($this->state->getAreaCode() == \Magento\Framework\App\Area::AREA_ADMINHTML) {
                // in admin area
                /** @var \Magento\Framework\App\RequestInterface $request */
                $request = $this->request;
                $this->_currentWebsite = (int) $request->getParam('website', 0);
            } else {
                // frontend area
                $this->_currentWebsite = true; // get current store from the store resolver
            }
        }


        return $this->_currentWebsite;
    }

    /**
     * @return int
     */
    public function getStoreId() {
        $websiteId = $this->resolveCurrentWebsiteId();
        if (is_numeric($websiteId)) {
            return $this->storeManager->getWebsite($websiteId)
                            ->getDefaultGroup()
                            ->getDefaultStoreId();
        }
        return $this->storeManager->getStore()->getStoreId();
    }

    public function getCurrencyCode() {
        return $this->storeManager->getStore()->getCurrentCurrency()->getCurrencyCode();
    }
    public function getCurrencyRates(){
//        return 'nada';
        $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/nada/test.log');
        $logger = new \Zend\Log\Logger();
        $logger->addWriter($writer);
        $ratesCollection= $this->collectionFactory->create()
                          ->addFieldToFilter('base_currency_code', $this->getCurrencyCode())
                          ->addFieldToFilter('service_id', $this->getCurrencyServiceId($this->getStoreId()))
                          ->addFieldToFilter('stripe_currency_code', $this->getStripeCurrency())
                          ->getLastItem();
       $message= $this->getConversionMessage();
       $message=__($message,$ratesCollection['base_currency_code'],$ratesCollection['stripe_currency_code'],$ratesCollection['rate']);
       $logger->info($message);
//       $message= str_replace('%1', $ratesCollection['base_currency_code'],$message);
//       $message=str_replace('%2', $ratesCollection['stripe_currency_code'],$message);
//       $message= str_replace('%3', $ratesCollection['rate'],$message);
       
       $logger->info($message);
       return $message;
//        return 'rate of exchange currency from '.$ratesCollection['base_currency_code']. ' to '. $ratesCollection['stripe_currency_code'] .' is ' .$ratesCollection['rate'] .' for each one.' ;      
//        return $ratesCollection->getData();
//        $rates = $this->currencyFactory->load($this->getCurrencyServiceId())->getData();
//         $logger->info(print_r($ratesCollection,true));
//         return 'nada';

   
       }

}
