<?php

namespace NovaMinds\StripeAllCurrencies\Plugin\Ui;

use Magento\Vault\Api\Data\PaymentTokenInterface;
use Magento\Vault\Model\Ui\TokenUiComponentInterface;
use Magento\Vault\Model\Ui\TokenUiComponentProviderInterface;
use Magento\Vault\Model\Ui\TokenUiComponentInterfaceFactory;
use Magento\Framework\UrlInterface;
use Stripeofficial\CreditCards\Helper\Helper;

class TokenUiComponentProviderPlugin 
{
    /**
     * @var TokenUiComponentInterfaceFactory
     */
    private $componentFactory;

    /**
     * @var \Magento\Framework\UrlInterface
     */
    private $urlBuilder;

    /**
     * @param TokenUiComponentInterfaceFactory $componentFactory
     * @param UrlInterface $urlBuilder
     */
    public function __construct(
        TokenUiComponentInterfaceFactory $componentFactory,
        UrlInterface $urlBuilder
    ) {
        $this->componentFactory = $componentFactory;
        $this->urlBuilder = $urlBuilder;
    }

    /**
     * Get UI component for token
     * @param PaymentTokenInterface $paymentToken
     * @return TokenUiComponentInterface
     */
    public function afterGetComponentForToken(\Stripeofficial\CreditCards\Model\Ui\TokenUiComponentProvider $subject, $result,PaymentTokenInterface $paymentToken)
    {
        
             $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/nada/test.log');
        $logger = new \Zend\Log\Logger();
        $logger->addWriter($writer);
        $jsonDetails = json_decode($paymentToken->getTokenDetails() ?: '{}', true);
        $logger->info('Sk');
//           $logger->info(ConfigProvider::CODE);
           try{
                     $result = $this->componentFactory->create(
            [
                'config' => [
                    'code' => Helper::VAULT_CODE,
                    'nonceUrl' => $this->getNonceRetrieveUrl(),
                    TokenUiComponentProviderInterface::COMPONENT_DETAILS => $jsonDetails,
                    TokenUiComponentProviderInterface::COMPONENT_PUBLIC_HASH => $paymentToken->getPublicHash()
                ],
                'name' => 'NovaMinds_StripeAllCurrencies/js/view/payment/vault'
            ]
        );
               
           } catch (Exception $e){
               die('hawk');
                $logger->info($e->getMessage());
           }
  
   $logger->info('after Sook');
        return $result;
    }

    /**
     * Get url to retrieve payment method nonce
     * @return string
     */
    private function getNonceRetrieveUrl()
    {
        return $this->urlBuilder->getUrl(\Stripeofficial\CreditCards\Model\Ui\ConfigProvider::CODE . '/payment/getnonce', ['_secure' => true]);
    }
}
