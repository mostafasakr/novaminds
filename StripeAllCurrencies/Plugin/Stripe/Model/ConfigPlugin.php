<?php

namespace NovaMinds\StripeAllCurrencies\Plugin\Stripe\Model;

use Magento\Stripe\Model\Config;
use NovaMinds\StripeAllCurrencies\Helper\Data;

/**
 * Class ConfigPlugin
 *
 * @package NovaMinds\StripeAllCurrencies\Model\Plugin\Stripe
 */
class ConfigPlugin
{
    /** @var \NovaMinds\StripeAllCurrencies\Helper\Data $helper */
    protected $helper;

    /**
     * ConfigPlugin constructor.
     *
     * @param \NovaMinds\StripeAllCurrencies\Helper\Data $helper
     */
    public function __construct(Data $helper)
    {
        $this->helper = $helper;
    }

    /**
     * Check whether specified currency code is supported
     *
     * @param \Magento\Stripe\Model\Config $config
     * @param                              $result
     * @return bool
     */
    public function afterIsCurrencyCodeSupported(Config $config, $result)
    {
        if (!$result && $this->helper->isModuleEnabled()) {
            $result = true;
        }

        return $result;
    }
}
