<?php

namespace NovaMinds\StripeAllCurrencies\Plugin\Stripe\Model\Api;

use Magento\Stripe\Model\Api\Nvp;
use NovaMinds\StripeAllCurrencies\Helper\Data;
use NovaMinds\StripeAllCurrencies\Model\CurrencyServiceFactory;

/**
 * Class NvpPlugin
 *
 * @package NovaMinds\StripeAllCurrencies\Model\Plugin\Stripe\Api
 */
class NvpPlugin
{
    /** @var \NovaMinds\StripeAllCurrencies\Helper\Data $helper */
    protected $helper;

    /** @var \NovaMinds\StripeAllCurrencies\Model\CurrencyServiceFactory $currencyServiceFactory */
    protected $currencyServiceFactory;

    /** @var \NovaMinds\StripeAllCurrencies\Model\CurrencyService\CurrencyServiceInterface|null $currencyService */
    protected $currencyService = null;

    /**
     * NvpPlugin constructor.
     *
     * @param \NovaMinds\StripeAllCurrencies\Helper\Data                  $helper
     * @param \NovaMinds\StripeAllCurrencies\Model\CurrencyServiceFactory $currencyServiceFactory
     */
    public function __construct(
        Data $helper,
        CurrencyServiceFactory $currencyServiceFactory
    ) {
        $this->helper = $helper;
        $this->currencyServiceFactory = $currencyServiceFactory;
    }

    /**
     * @TODO-zamoroka: remove this plugin. Find another solution
     * used in Magento\Stripe\Model\Express\Checkout::start()
     *
     * @param \Magento\Stripe\Model\Api\Nvp $nvp
     * @param                               $key
     * @param null                          $value
     * @return array
     */
    public function beforeSetData(Nvp $nvp, $key, $value = null)
    {
        if ($this->helper->isModuleEnabled()) {
            switch ($key) {
                case 'amount':
                    $value = $this->getCurrencyService()->exchange($value);
                    break;
                case 'currency_code':
                    $value = $this->helper->getStripeCurrency();
                    break;
                default:
                    break;
            }
        }

        return [$key, $value];
    }

    /**
     * @return false|null|\NovaMinds\StripeAllCurrencies\Model\CurrencyService\CurrencyServiceInterface
     */
    public function getCurrencyService()
    {
        if (!$this->currencyService) {
            $this->currencyService = $this->currencyServiceFactory->load($this->helper->getCurrencyServiceId());
        }

        return $this->currencyService;
    }
}
