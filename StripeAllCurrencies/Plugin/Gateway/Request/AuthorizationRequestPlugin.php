<?php


namespace NovaMinds\StripeAllCurrencies\Plugin\Gateway\Request;


use Magento\Framework\Message\ManagerInterface;
use Magento\Quote\Api\Data\CartInterface;
use Magento\Quote\Model\Quote\Item;
use Magento\Quote\Model\QuoteRepository;
use Psr\Log\LoggerInterface;
use Magento\Payment\Gateway\Data\PaymentDataObjectInterface;
use NovaMinds\StripeAllCurrencies\Helper\Data;
use NovaMinds\StripeAllCurrencies\Model\CurrencyServiceFactory;
use NovaMinds\StripeAllCurrencies\Preference\Payment\Model\Cart\SalesModel\Quote as SalesModelQuote;

/**
 * Description of AuthorizationRequestPlugin
 *
 * @author hema
 */
class AuthorizationRequestPlugin {
   
    
        /** @var \NovaMinds\StripeAllCurrencies\Helper\Data $helper */
    protected $helper;

    /** @var \NovaMinds\StripeAllCurrencies\Model\CurrencyServiceFactory $currencyServiceFactory */
    protected $currencyServiceFactory;

    /** @var \NovaMinds\StripeAllCurrencies\Model\CurrencyService\CurrencyServiceInterface|null $currencyService */
    protected $currencyService = null;

    /** @var \Psr\Log\LoggerInterface $logger */
    protected $logger;

    /** @var \Magento\Framework\Message\ManagerInterface */
    protected $messageManager;

    /**
     * QuoteRepositoryPlugin constructor.
     *
     * @param \NovaMinds\StripeAllCurrencies\Helper\Data                  $helper
     * @param \NovaMinds\StripeAllCurrencies\Model\CurrencyServiceFactory $currencyServiceFactory
     * @param \Psr\Log\LoggerInterface                                   $logger
     * @param \Magento\Framework\Message\ManagerInterface                $messageManager
     */
    public function __construct(
        Data $helper,
        CurrencyServiceFactory $currencyServiceFactory,
        LoggerInterface $logger,
        ManagerInterface $messageManager
    ) {
        $this->helper = $helper;
        $this->logger = $logger;
        $this->messageManager = $messageManager;
        $this->currencyServiceFactory = $currencyServiceFactory;
    }
    
    
    	public function afterBuild(\Stripeofficial\CreditCards\Gateway\Request\AuthorizationRequest $subject, $result, array $buildSubject) {
            

        if (!isset($buildSubject['payment']) || !$buildSubject['payment'] instanceof PaymentDataObjectInterface
        ) {

            throw new \InvalidArgumentException('Payment data object should be provided sakr');
        }

        /** @var PaymentDataObjectInterface $paymentDO */
        $paymentDO = $buildSubject['payment'];
        /** @var Order $order */
        $order = $paymentDO->getOrder();
        /** @var Order\Payment $payment */
        $payment = $paymentDO->getPayment();
        $orderModel = $payment->getOrder();
        $quote = $orderModel->getQuote();

        if ($this->helper->isModuleEnabled() && in_array(
                        $orderModel->getPayment()->getMethod(), $this->helper->getStripePaymentMethods()
                )) {
            
            try {
                

                if ($this->helper->getStripeCurrency()&& $this->getCurrencyService()->exchange($result['AMOUNT'])) {
                    
                    $result['AMOUNT'] = $this->getCurrencyService()->exchange($result['AMOUNT'],2);
                     $logger->info('we are in'.$this->helper->getStripeCurrency());
                    $result['CURRENCY_CODE'] = $this->helper->getStripeCurrency();
                }
            } catch (Exception $exc) {
                $this->logger->critical($exc->getMessage());
            }
        }
        return $result;
    }
    
    
        /**
     * @return false|null|\NovaMinds\StripeAllCurrencies\Model\CurrencyService\CurrencyServiceInterface
     */
    public function getCurrencyService()
    {
        if (!$this->currencyService) {
            $this->currencyService = $this->currencyServiceFactory->load($this->helper->getCurrencyServiceId());
        }

        return $this->currencyService;
    }
    
    


}
