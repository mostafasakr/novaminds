<?php


namespace NovaMinds\StripeAllCurrencies\Plugin\Gateway\Request;


use Magento\Framework\Message\ManagerInterface;
use Magento\Quote\Api\Data\CartInterface;
use Magento\Quote\Model\Quote\Item;
use Magento\Quote\Model\QuoteRepository;
use Psr\Log\LoggerInterface;
use Magento\Payment\Gateway\Data\PaymentDataObjectInterface;
use NovaMinds\StripeAllCurrencies\Helper\Data;
use NovaMinds\StripeAllCurrencies\Model\CurrencyServiceFactory;
use NovaMinds\StripeAllCurrencies\Preference\Payment\Model\Cart\SalesModel\Quote as SalesModelQuote;

/**
 * Description of AuthorizationRequestPlugin
 *
 * @author hema
 */
class VoidRequestPlugin {
   
    
        /** @var \NovaMinds\StripeAllCurrencies\Helper\Data $helper */
    protected $helper;

    /** @var \NovaMinds\StripeAllCurrencies\Model\CurrencyServiceFactory $currencyServiceFactory */
    protected $currencyServiceFactory;

    /** @var \NovaMinds\StripeAllCurrencies\Model\CurrencyService\CurrencyServiceInterface|null $currencyService */
    protected $currencyService = null;

    /** @var \Psr\Log\LoggerInterface $logger */
    protected $logger;

    /** @var \Magento\Framework\Message\ManagerInterface */
    protected $messageManager;

    /**
     * QuoteRepositoryPlugin constructor.
     *
     * @param \NovaMinds\StripeAllCurrencies\Helper\Data                  $helper
     * @param \NovaMinds\StripeAllCurrencies\Model\CurrencyServiceFactory $currencyServiceFactory
     * @param \Psr\Log\LoggerInterface                                   $logger
     * @param \Magento\Framework\Message\ManagerInterface                $messageManager
     */
    public function __construct(
        Data $helper,
        CurrencyServiceFactory $currencyServiceFactory,
        LoggerInterface $logger,
        ManagerInterface $messageManager
    ) {
        $this->helper = $helper;
        $this->logger = $logger;
        $this->messageManager = $messageManager;
        $this->currencyServiceFactory = $currencyServiceFactory;
    }
    
    
    	public function afterBuild(\Stripeofficial\CreditCards\Gateway\Request\AuthorizationRequest $subject, $result, array $buildSubject) {

        if (!isset($buildSubject['payment']) || !$buildSubject['payment'] instanceof PaymentDataObjectInterface
        ) {

            throw new \InvalidArgumentException('Payment data object should be provided sakr');
        }

       /** @var PaymentDataObjectInterface $paymentDO */
        $paymentDO = $buildSubject['payment'];

        /** @var Payment $payment */
        $payment = $paymentDO->getPayment();
        $order = $paymentDO->getOrder();
        /** @var Order\Payment $payment */
        $payment = $paymentDO->getPayment();
        $orderModel = $payment->getOrder();
        $storeId = $orderModel->getStoreId();
 
//        $transactionId = $payment->getLastTransId();
//        $amount = $payment->getBaseAmountAuthorized();
        if ($this->helper->isModuleEnabled($storeId)) {
            try {

                if ($this->helper->getStripeCurrency($storeId) && $this->getCurrencyService($storeId)->exchange($result['REFUND_AMOUNT'], 2, $storeId)) {

                    $result['CURRENCY_CODE'] = $this->helper->getStripeCurrency($storeId);
                }
            } catch (Exception $exc) {
                $this->logger->critical($exc->getMessage());
            }
        }
        return $result;
    }

    /**
     * @return false|null|\NovaMinds\StripeAllCurrencies\Model\CurrencyService\CurrencyServiceInterface
     */
    public function getCurrencyService($storeId = null) {
        if (!$this->currencyService) {
            $this->currencyService = $this->currencyServiceFactory->load($this->helper->getCurrencyServiceId($storeId));
        }

        return $this->currencyService;
    }

}
