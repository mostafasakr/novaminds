<?php

namespace NovaMinds\StripeAllCurrencies\Plugin\Sales\Model;

use Magento\Framework\Message\ManagerInterface;
use Magento\Sales\Api\Data\OrderInterface;
use Magento\Sales\Model\Order\Item;
use Magento\Sales\Model\OrderRepository;
use Psr\Log\LoggerInterface;
use NovaMinds\StripeAllCurrencies\Helper\Data;
use NovaMinds\StripeAllCurrencies\Model\CurrencyServiceFactory;
use NovaMinds\StripeAllCurrencies\Preference\Payment\Model\Cart\SalesModel\Order as SalesModelOrder;

/**
 * Class OrderPlugin
 *
 * @package NovaMinds\StripeAllCurrencies\Plugin\Sales\Model
 */
class OrderRepositoryPlugin
{
    /** @var \NovaMinds\StripeAllCurrencies\Helper\Data $helper */
    protected $helper;

    /** @var \NovaMinds\StripeAllCurrencies\Model\CurrencyServiceFactory $currencyServiceFactory */
    protected $currencyServiceFactory;

    /** @var \NovaMinds\StripeAllCurrencies\Model\CurrencyService\CurrencyServiceInterface|null $currencyService */
    protected $currencyService = null;

    /** @var \Psr\Log\LoggerInterface $logger */
    protected $logger;

    /** @var \Magento\Framework\Message\ManagerInterface */
    protected $messageManager;

    /**
     * OrderPlugin constructor.
     *
     * @param \NovaMinds\StripeAllCurrencies\Helper\Data                  $helper
     * @param \NovaMinds\StripeAllCurrencies\Model\CurrencyServiceFactory $currencyServiceFactory
     * @param \Psr\Log\LoggerInterface                                   $logger
     * @param \Magento\Framework\Message\ManagerInterface                $messageManager
     */
    public function __construct(
        Data $helper,
        CurrencyServiceFactory $currencyServiceFactory,
        LoggerInterface $logger,
        ManagerInterface $messageManager
    ) {
        $this->helper = $helper;
        $this->logger = $logger;
        $this->messageManager = $messageManager;
        $this->currencyServiceFactory = $currencyServiceFactory;
    }

    /**
     * @param \Magento\Sales\Model\OrderRepository                   $orderRepository
     * @param \Magento\Sales\Api\Data\OrderInterface|SalesModelOrder $entity
     * @return array
     */
    public function beforeSave(OrderRepository $orderRepository, OrderInterface $entity)
    {
        if ($this->helper->isModuleEnabled() && $this->helper->isOrderPlacedByStripe($entity)) {
            try {
                foreach ($entity->getAllItems() as $item) {
                    $amt = $this->geStripeAmtFromItem('item_price', $entity, $item);
                    $amtTotal = $this->geStripeAmtFromItem('item_row_total', $entity, $item);
                    $item->setStripePrice($amt);
                    $item->setStripeRowTotal($amtTotal);
                }

                $entity->setStripeSubtotal($this->geStripeAmtFromItem('subtotal', $entity));
                $entity->setStripeGrandTotal($this->geStripeAmtFromItem('grand_total', $entity));

                $entity->setStripeShippingAmount($this->geStripeAmtFromItem('shipping', $entity));
                $entity->setStripeDiscountAmount($this->geStripeAmtFromItem('discount', $entity));
                $entity->setStripeTaxAmount($this->geStripeAmtFromItem('tax', $entity));

                $entity->setStripeRate($this->getCurrencyService()->exchange(1));
                $entity->setStripeCurrencyCode($this->helper->getStripeCurrency());
            } catch (\Exception $exception) {
                $this->logger->critical($exception->getMessage());
            }
        }

        return [$entity];
    }

    /**
     * @return false|null|\NovaMinds\StripeAllCurrencies\Model\CurrencyService\CurrencyServiceInterface
     */
    public function getCurrencyService()
    {
        if (!$this->currencyService) {
            $this->currencyService = $this->currencyServiceFactory->load($this->helper->getCurrencyServiceId());
        }

        return $this->currencyService;
    }

    /**
     * @param string                                 $code
     * @param \Magento\Sales\Api\Data\OrderInterface $order
     * @param \Magento\Sales\Model\Order\Item|null   $item
     * @return float
     */
    public function geStripeAmtFromItem($code, OrderInterface $order, Item $item = null)
    {
        $amt = 0;

        /** @var \Magento\Sales\Model\Order\Address $address */
        $address = $order->getIsVirtual() ? $order->getBillingAddress() : $order->getShippingAddress();

        try {
            switch ($code) {
                case 'subtotal':
                    $amt = $order->getBaseSubtotal();
                    break;
                case 'grand_total':
                    $amt = $order->getBaseGrandTotal();
                    break;
                case 'shipping':
                    $amt = $address->getBaseShippingAmount();
                    break;
                case 'discount':
                    $amt = $address->getBaseDiscountAmount();
                    break;
                case 'tax':
                    $amt = $address->getBaseTaxAmount();
                    break;
                case 'item_price':
                    $amt = $item->getBasePrice();
                    break;
                case 'item_row_total':
                    $amt = $item->getBaseRowTotal();
                    break;
                default:
                    break;
            }
        } catch (\Throwable $t) {
            $amt = 0;
            $this->logger->critical($t->getMessage());
            $this->messageManager->addErrorMessage(__('Can`t calculate stripe amount'));
        }

        return $this->getCurrencyService()->exchange($amt);
    }
}
