<?php

namespace NovaMinds\StripeAllCurrencies\Plugin\Sales\Helper;

use Magento\Sales\Helper\Admin;
use Magento\Sales\Model\Order;
use NovaMinds\StripeAllCurrencies\Helper\Data;

/**
 * Class AdminPlugin
 *
 * @package NovaMinds\StripeAllCurrencies\Plugin\Sales\Helper
 */
class AdminPlugin
{
    /** @var \NovaMinds\StripeAllCurrencies\Helper\Data $helper */
    protected $helper;

    /**
     * OrderPlugin constructor.
     *
     * @param \NovaMinds\StripeAllCurrencies\Helper\Data                  $helper
     * @param \NovaMinds\StripeAllCurrencies\Model\CurrencyServiceFactory $currencyServiceFactory
     * @param \Psr\Log\LoggerInterface                                   $logger
     * @param \Magento\Framework\Message\ManagerInterface                $messageManager
     */
    public function __construct(Data $helper)
    {
        $this->helper = $helper;
    }

    /**
     * @param \Magento\Sales\Helper\Admin $adminHelper
     * @param                             $dataObject
     * @param                             $basePrice
     * @param                             $price
     * @param bool                        $strong
     * @param string                      $separator
     * @return array
     */
    public function beforeDisplayPrices(
        Admin $adminHelper,
        $dataObject,
        $basePrice,
        $price,
        $strong = false,
        $separator = '<br/>'
    ) {
        $order = ($dataObject instanceof Order) ? $dataObject : $dataObject->getOrder();

        if ($price && $order && $this->helper->isOrderPlacedByStripe($order)) {
            $price = $order->getStripeRate() * (float)$price;
            $order->setOrderCurrencyCode($order->getStripeCurrencyCode());
        }

        return [$order, $basePrice, $price, $strong, $separator];
    }
}
