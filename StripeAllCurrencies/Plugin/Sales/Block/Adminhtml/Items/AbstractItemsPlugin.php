<?php

namespace NovaMinds\StripeAllCurrencies\Plugin\Sales\Block\Adminhtml\Items;

use Magento\Sales\Block\Adminhtml\Items\AbstractItems;
use NovaMinds\StripeAllCurrencies\Helper\Data;

/**
 * Class AbstractItemsPlugin
 *
 * @package NovaMinds\StripeAllCurrencies\Plugin\Sales\Block\Adminhtml\Items
 */
class AbstractItemsPlugin
{
    /** @var \NovaMinds\StripeAllCurrencies\Helper\Data $helper */
    protected $helper;

    /**
     * OrderPlugin constructor.
     *
     * @param \NovaMinds\StripeAllCurrencies\Helper\Data $helper
     */
    public function __construct(Data $helper)
    {
        $this->helper = $helper;
    }

    /**
     * @param \Magento\Sales\Block\Adminhtml\Items\AbstractItems $abstractItems
     * @param                                                    $basePrice
     * @param                                                    $price
     * @param int                                                $precision
     * @param bool                                               $strong
     * @param string                                             $separator
     * @return array
     */
    public function beforeDisplayRoundedPrices(
        AbstractItems $abstractItems,
        $basePrice,
        $price,
        $precision = 2,
        $strong = false,
        $separator = '<br />'
    ) {
        $order = $abstractItems->getOrder();

        if ($price && $order && $this->helper->isOrderPlacedByStripe($order)) {
            $price = $order->getStripeRate() * (float)$price;
            $order->setOrderCurrencyCode($order->getStripeCurrencyCode());
        }

        return [$basePrice, $price, $precision, $strong, $separator];
    }
}
