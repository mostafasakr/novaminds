<?php

namespace NovaMinds\StripeAllCurrencies\Plugin\Config\Model;

use Magento\Config\Model\Config;
use NovaMinds\StripeAllCurrencies\Helper\Data;
use NovaMinds\StripeAllCurrencies\Model\CurrencyServiceFactory;
use NovaMinds\StripeAllCurrencies\Model\RatesFactory;

/**
 * Class ConfigPlugin
 */
class ConfigPlugin
{
    /** @var \NovaMinds\StripeAllCurrencies\Helper\Data $_helper */
    protected $_helper;

    /** @var \NovaMinds\StripeAllCurrencies\Model\CurrencyServiceFactory $currencyServiceFactory */
    protected $currencyServiceFactory;

    /** @var \NovaMinds\StripeAllCurrencies\Model\RatesFactory $ratesFactory */
    protected $ratesFactory;

    /**
     * ConfigPlugin constructor.
     *
     * @param \NovaMinds\StripeAllCurrencies\Helper\Data                  $helper
     * @param \NovaMinds\StripeAllCurrencies\Model\CurrencyServiceFactory $currencyServiceFactory
     * @param \NovaMinds\StripeAllCurrencies\Model\RatesFactory           $ratesFactory
     */
    public function __construct(
        Data $helper,
        CurrencyServiceFactory $currencyServiceFactory,
        RatesFactory $ratesFactory
    ) {
        $this->_helper = $helper;
        $this->currencyServiceFactory = $currencyServiceFactory;
        $this->ratesFactory = $ratesFactory;
    }

    /**
     * @param Config $config
     * @param Config $result
     * @return Config
     */
    public function afterSave(Config $config, $result)
    {
        if ($config->getSection() === 'novaminds_stripeallcurrencies') {
            /** @var \NovaMinds\StripeAllCurrencies\Model\CurrencyService\CurrencyServiceInterface $currencyService */
            $currencyService = $this->currencyServiceFactory->load($this->_helper->getCurrencyServiceId());

            /** @var \NovaMinds\StripeAllCurrencies\Model\Rates $ratesModel */
            $ratesModel = $this->ratesFactory->create();
            $ratesModel->updateRateFromService($currencyService);
            $ratesModel->save();
        }

        return $result;
    }
}
