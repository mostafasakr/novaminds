<?php

namespace NovaMinds\Fawry\Controller\Fawry;

class ServerToServer extends \NovaMinds\Fawry\Controller\Checkout
{
    public function execute()
    {
//        $failedOrCanceled = $this->getRequest()->getParam('failedOrCanceled');
        $orderId = $this->getRequest()->getParam('MerchnatRefNo');
        $OrderStatus = $this->getRequest()->getParam('OrderStatus');
        $order = $this->getOrderById($orderId);
        $fawryID = $this->getRequest()->getParam('fawryRefNo');
       
        
        if(!$failedOrCanceled) {
            
            $this->getHelper()->processReturnOrder($order , $fawryID);
            return;
        }
        else {
            $this->getHelper()->orderFailed($order);
            $this->_checkoutSession->restoreQuote();
        }
        
        switch ($OrderStatus){
            case "New":
                $this->getHelper()->processReturnOrder($order , $fawryID);
                break;
            case "PAID":
                $this->getHelper()->processServerOrder($order , $fawryID);
                break;
            case "CANCELED":
                $this->getHelper()->orderFailed($order);
                break;
            case "EXPIRED":
                $this->getHelper()->orderFailed($order);
                break;
                            
        }
        
    }

}
