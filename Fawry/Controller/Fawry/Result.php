<?php

namespace NovaMinds\Fawry\Controller\Fawry;

use Magento\Checkout\Model\Session;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Controller\ResultFactory;
use Magento\Sales\Model\Order;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Store\Model\ScopeInterface;

class Result extends \Magento\Framework\App\Action\Action
{
      protected $_config;
public function __construct(
        Context $context,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig, 
        Session $checkoutSession, 
         ScopeConfigInterface $configInterface,
        \Magento\Framework\Locale\Resolver $store,
        \Magento\Framework\UrlInterface $urlBuilder,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory, 
        \Magento\Directory\Model\CountryFactory $countryFactory
    ) {
        parent::__construct($context);
        $this->scopeConfig = $scopeConfig; //Used for getting data from System/Admin config
        $this->checkoutSession = $checkoutSession; //Used for getting the order: $order = $this->checkoutSession->getLastRealOrder(); And other order data like ID & amount
        $this->store = $store; //Used for getting store locale if needed $language_code = $this->store->getLocale();
        $this->urlBuilder = $urlBuilder; //Used for creating URLs to other custom controllers, for example $success_url = $this->urlBuilder->getUrl('frontname/path/action');
        $this->_config = $configInterface;
        $this->resultJsonFactory = $resultJsonFactory; //Used for returning JSON data to the afterPlaceOrder function ($result = $this->resultJsonFactory->create(); return $result->setData($post_data);)
    }

public function execute()
    {
    $post = $this->getRequest()->getParams();
    var_dump($post);
    die;
    //Your custom code for getting the data the payment provider needs
    //Structure your return data so the form-builder.js can build your form correctly
    $post_data = array(
        'action' => $this->getGateURL(),
        'fields' => array (
            'merchant_code' => $this->_getConfig('merchant_code'),
            'customerName' => 'Mostafa Sakr',
            'mobile' => 'Mostafa Sakr',
            'mobile' => '01009361378',
            'email' => 'abc@fawry.com',
            'locale' => 'ar_eg',
            'merchantRefNum' => '10000455',
          'account_number' => $this->_getConfig('account_number'),
          'security_key' => $this->_getConfig('security_key'),
            //add all the fields you need
        )
    );
    $result = $this->resultJsonFactory->create();

    return $result->setData($post_data); //return data in JSON format
}

  public function getGateURL() {
    if ($this->_isTestMode()) {
      return $this->_getTestBaseUrl();
    }
    return $this->_getProductionBaseUrl();
  }

  protected function _isTestMode() {
    return $this->_getConfig('test_active');
  }
  protected function _getTestBaseUrl() {
       return 'https://www.atfawry.com/ECommercePlugin/fawry_plugin_auto.jsp';
//    return 'https://www.atfawry.fawrystaging.com/ECommercePlugin/fawry_plugin_auto.jsp';
  }
  protected function _getProductionBaseUrl() {
    return 'https://www.atfawry.com/ECommercePlugin/fawry_plugin_auto.jsp';
  }



  protected function _getConfig($value) {
    return $this->_config->getValue('payment/fawry/' . $value, ScopeInterface::SCOPE_STORE);
  }

}
