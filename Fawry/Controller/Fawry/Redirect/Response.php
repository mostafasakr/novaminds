<?php

namespace NovaMinds\Fawry\Controller\Fawry\Redirect;

class Response extends \NovaMinds\Fawry\Controller\Checkout
{
    public function execute()
    {
        $statues = $this->getRequest()->getParam('OrderStatus');
        $orderId = $this->getRequest()->getParam('merchantRefNum');
        $order = $this->getOrderById($orderId);
       
        
     switch ($statues) {
    case 'PAID':
        $this->getHelper()->processServerOrder($order , $fawryID);
        break;
    case 'CANCELED':
        $this->getHelper()->orderFailed($order);
        break;
    case 'EXPIRED':
        $this->getHelper()->orderFailed($order);
        break;
  
}
//        if(!$failedOrCanceled) {
//            $fawryID = $this->getRequest()->getParam('fawryRefNo');
//            $this->getHelper()->processReturnOrder($order , $fawryID);
//            $returnUrl = $this->getHelper()->getUrl('checkout/onepage/success');
//        }
//        else {
//            $this->getHelper()->orderFailed($order);
//            $this->_checkoutSession->restoreQuote();
//            $message = __('Error: Payment Failed, Please check your payment details and try again.');
//            $this->messageManager->addError( $message );
//            $returnUrl = $this->getHelper()->getUrl('checkout/cart');
//        }
//        
//        $this->orderRedirect($order, $returnUrl);
        return TRUE;
    }

}
