<?php

namespace NovaMinds\Fawry\Controller\Fawry;

class MerchantPageResponse extends \NovaMinds\Fawry\Controller\Checkout
{
    public function execute()
    {
        $failedOrCanceled = $this->getRequest()->getParam('failedOrCanceled');
        $orderId = $this->getRequest()->getParam('merchantRefNum');
        $order = $this->getOrderById($orderId);
        $returnUrl = $this->getHelper()->getUrl('checkout/onepage/success');
       
        if($orderId && $orderId != 'undefined'){
        if(!$failedOrCanceled) {
            $fawryID = $this->getRequest()->getParam('fawryRefNo');
            $this->getHelper()->processReturnOrder($order , $fawryID);
//            $returnUrl = $this->getHelper()->getUrl('checkout/onepage/success');
        }
        else {
            $this->getHelper()->orderFailed($order);
            $this->_checkoutSession->restoreQuote();
            $message = __('Error: Payment Failed, Please check your payment details and try again.');
            $this->messageManager->addError( $message );
            $returnUrl = $this->getHelper()->getUrl('checkout/cart');
        }
        
                $this->orderRedirect($order, $returnUrl);

        }else{
                  $this->redirectWithoutOrder( $returnUrl);
        }
        
  
    }

}
