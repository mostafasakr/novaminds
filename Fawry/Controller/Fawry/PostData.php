<?php

namespace NovaMinds\Fawry\Controller\Fawry;

use Magento\Checkout\Model\Session;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Controller\ResultFactory;
use Magento\Sales\Model\Order;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Store\Model\ScopeInterface;

class PostData extends \Magento\Framework\App\Action\Action {

    protected $_config;
    protected $_lastOrder;

    public function __construct(
    Context $context, \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig, Session $checkoutSession, ScopeConfigInterface $configInterface, \Magento\Framework\Locale\Resolver $store, \Magento\Framework\UrlInterface $urlBuilder, \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory, \Magento\Directory\Model\CountryFactory $countryFactory
    ) {
        parent::__construct($context);
        $this->scopeConfig = $scopeConfig; //Used for getting data from System/Admin config
        $this->checkoutSession = $checkoutSession; //Used for getting the order: $order = $this->checkoutSession->getLastRealOrder(); And other order data like ID & amount
        $this->store = $store; //Used for getting store locale if needed $language_code = $this->store->getLocale();
        $this->urlBuilder = $urlBuilder; //Used for creating URLs to other custom controllers, for example $success_url = $this->urlBuilder->getUrl('frontname/path/action');
        $this->_config = $configInterface;
        $this->resultJsonFactory = $resultJsonFactory; //Used for returning JSON data to the afterPlaceOrder function ($result = $this->resultJsonFactory->create(); return $result->setData($post_data);)
        $this->_lastOrder = $this->checkoutSession->getLastRealOrder();
    }

    public function execute() {
        //Your custom code for getting the data the payment provider needs
        //Structure your return data so the form-builder.js can build your form correctly
        $post_data = array(
//        'action' => $this->getGateURL(),
//        'fields' => array (
            'merchant' => $this->_getConfig('merchant_code'),
            'customerName' => $this->_getCustomerName(),
            'mobile' => $this->_getTel(),
            'amount'=> $this->_getGrandTotal(),
            'redirectToURL' => $this->urlBuilder->getUrl('fawry/fawry/merchantpageresponse'),
            'email' => $this->_getCustomerEmail(),
            'orderExpiry' => '200',
            'orderDesc' => __('Herbsgate payments'),
            'locale' => 'ar-eg',
            'merchantRefNum' => $this->_getOrderId(),
            'order' => json_encode($this->_getItems()),
//            'account_number' => $this->_getConfig('account_number'),
//            'security_key' => $this->_getConfig('security_key'),
//        )
        );

        $order_data = $this->checkoutSession->getLastRealOrder()->getData();

        $urlData = array(
            'url' => $this->getGateURL() . '?' . http_build_query($post_data),
            'order' => $order_data,
            'address' => $this->_lastOrder->getdata(),
            'items' => $this->_getItems()
        );

        $result = $this->resultJsonFactory->create();

        return $result->setData($urlData); //return data in JSON format
    }

    public function getGateURL() {
        if ($this->_isTestMode()) {
            return $this->_getTestBaseUrl();
        }
        return $this->_getProductionBaseUrl();
    }

    protected function _isTestMode() {
        return $this->_getConfig('test_active');
    }

    protected function _getTestBaseUrl() {
        return 'https://atfawry.fawrystaging.com/ECommercePlugin/fawry_plugin_auto.jsp';
//    return 'https://www.atfawry.fawrystaging.com/ECommercePlugin/fawry_plugin_auto.jsp';
    }

    protected function _getProductionBaseUrl() {
        return 'https://www.atfawry.com/ECommercePlugin/fawry_plugin_auto.jsp';
    }

    protected function _getCustomerEmail() {
        return $this->_lastOrder->getCustomerEmail();
    }

    protected function _getCustomerProfileId() {
        if ($this->_lastOrder->getData('customer_is_guest')) {
            return $this->_lastOrder->getCustomerEmail();
        }
        return $this->_lastOrder->getData('customer_id');
    }

    protected function _getTel() {
        return $this->_lastOrder->getBillingAddress()->getTelephone();
    }
    protected function _getGrandTotal() {
        return $this->_lastOrder->getGrandTotal();
    }

    protected function _getItems() {
        $order = array();
//        $items = $this->_lastOrder->getAllItems();
//        foreach ($this->_lastOrder->getAllItems() as $item) {
//            $itemData = array(
//                'productSKU' => $item->getData('sku'),
//                'description' => $item->getData('name'),
//                'price' => (float)$item->getData('price_incl_tax'),
//                'quantity' => (int)$item->getData('qty_ordered'),
//                'width' => '1',
//                'height' => '1',
//                'length' => '1',
//                'weight' => '1500',
//            );
//            $order[] = $itemData;
//        }
             $itemData = array(
                'productSKU' => 'total-grand',
                'description' => __('Grand Total'),
                'price' => $this->_getGrandTotal(),
                'quantity' => '1',
      
            );
              $order[] = $itemData;
        return $order;
    }

    protected function _getOrderId() {
        return $this->_lastOrder->getData('increment_id');
    }

    protected function _getCustomerName() {
        if ($this->_lastOrder->getData('customer_is_guest')) {
            return  $this->_lastOrder->getBillingAddress()->getData('firstname') . ' ' . $this->_lastOrder->getBillingAddress()->getData('lastname');
        }
        return $this->_lastOrder->getCustomerFirstname() . ' ' . $this->_lastOrder->getCustomerLastname();
    }

    protected function getGrandTotal() {

        return $this->_lastOrder->getGrandTotal();
    }

    protected function _getConfig($value) {
        return $this->_config->getValue('payment/fawry/' . $value, ScopeInterface::SCOPE_STORE);
    }

}
