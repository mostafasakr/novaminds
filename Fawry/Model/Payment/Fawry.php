<?php


namespace NovaMinds\Fawry\Model\Payment;

class Fawry extends \Magento\Payment\Model\Method\AbstractMethod
{

    protected $_code = "fawry";
    protected $_isOffline = true;

    public function isAvailable(
        \Magento\Quote\Api\Data\CartInterface $quote = null
    ) {
        return parent::isAvailable($quote);
    }
}
