<?php
/**
 * Pmclain_Stripe extension
 * NOTICE OF LICENSE
 *
 * This source file is subject to the OSL 3.0 License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/osl-3.0.php
 *
 * @category  Pmclain
 * @package   Pmclain_Stripe
 * @copyright Copyright (c) 2017-2018
 * @license   Open Software License (OSL 3.0)
 */
namespace NovaMinds\Fawry\Model\Ui;

use Magento\Checkout\Model\ConfigProviderInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Encryption\EncryptorInterface;
use Magento\Store\Model\ScopeInterface;

class ConfigProvider implements ConfigProviderInterface
{
  const CODE = 'fawry';

  protected $_config;
  protected $_encryptor;

  public function __construct(
    ScopeConfigInterface $configInterface,
    EncryptorInterface $encryptorInterface
  ){
    $this->_config = $configInterface;
    $this->_encryptor = $encryptorInterface;
  }

  public function getConfig()
  {
    return [
      'payment' => [
        self::CODE => [
          'gate_url' => $this->getGateURL(),
          'merchant_code' => $this->_getConfig('merchant_code'),
          'account_number' => $this->_getConfig('account_number'),
          'security_key' => $this->_getConfig('security_key'),
          'instructions' => $this->_getConfig('instructions'),
        ]
      ]
    ];
  }

  public function getGateURL() {
    if ($this->_isTestMode()) {
      return $this->_getTestBaseUrl();
    }
    return $this->_getProductionBaseUrl();
  }

  protected function _isTestMode() {
    return $this->_getConfig('test_active');
  }
  protected function _getTestBaseUrl() {
    return 'www.atfawry.fawrystaging.com/ECommercePlugin/fawry_plugin_auto.jsp';
  }
  protected function _getProductionBaseUrl() {
    return 'www.atfawry.com/ECommercePlugin/fawry_plugin_auto.jsp';
  }



  protected function _getConfig($value) {
    return $this->_config->getValue('payment/fawry/' . $value, ScopeInterface::SCOPE_STORE);
  }
}