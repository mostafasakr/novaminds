define(
    [
        'jquery',
        'Magento_Checkout/js/view/payment/default',
        'Magento_Checkout/js/model/quote',
        'Magento_Checkout/js/model/full-screen-loader',
        'Magento_Checkout/js/action/set-payment-information',
        'Magento_Checkout/js/action/place-order',
        
    ],
    
    function ($, Component, quote, fullScreenLoader, setPaymentInformationAction, placeOrder) {
        'use strict';
        return Component.extend({
            redirectAfterPlaceOrder: false, //This is important, so the customer isn't redirected to success.phtml by default
            defaults: {
                template: 'NovaMinds_Fawry/payment/fawry'
            },
            getMailingAddress: function () {
                return window.checkoutConfig.payment.checkmo.mailingAddress;
            },
            getCode: function() {
                return 'fawry';
            },
               isActive: function() {
                return true;
            },
            getDebug: function() {
                return window.checkoutConfig.payment;
            },
               // Overwrite properties / functions
            redirectAfterPlaceOrder: false,
            
            /**
             * @override
             */
//            placeOrder: function () {
//
//                var self = this;
//                var paymentData = quote.paymentMethod();
//                var messageContainer = this.messageContainer;
//                var cartItems = JSON.stringify(quote.getItems());
//                fullScreenLoader.startLoader();
//                this.isPlaceOrderActionAllowed(false);
//                $.when(setPaymentInformationAction(this.messageContainer, {
//                    'method': self.getCode()
//                })).done(function () {
//                    console.log(self.getDebug());
//                        $.when(placeOrder(paymentData, messageContainer)).done(function () {
////                            $.mage.redirect(window.checkoutConfig.payment.payfortFort.payfort_fort_sadad.redirectUrl);
//
//                            $.mage.redirect(window.checkoutConfig.payment.fawry.gate_url);
//                        });
//                }).fail(function () {
//                    self.isPlaceOrderActionAllowed(true);
//                }).always(function(){
//                    fullScreenLoader.stopLoader();
//                });
//            }

        });
    }
);
