define(
    [
        'uiComponent',
        'Magento_Checkout/js/model/payment/renderer-list'
    ],
    function (
        Component,
        rendererList
    ) {
        'use strict';
        rendererList.push(
            {
                type: 'fawry',
                component: 'NovaMinds_Fawry/js/view/payment/method-renderer/fawry-method'
            }
        );
        return Component.extend({});
    }
);